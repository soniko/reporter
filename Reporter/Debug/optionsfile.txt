%reportoptions{1,1}
char
name
%reportoptions{1,2}
char
amplitude
%reportoptions{1,3}
char
fitfile
%reportoptions{1,4}
char

%reportoptions{1,5}
char
ndb
%reportoptions{1,6}
char
ndbs.mat
%reportoptions{1,7}
char
format
%reportoptions{1,8}
char
PNG
%reportoptions{1,9}
char
folder
%reportoptions{1,10}
char
..\masstestdata
%reportoptions{1,11}
char
basename
%reportoptions{1,12}
char
pupilrep
%reportoptions{1,13}
char
res
%reportoptions{1,14}
char
300
%reportoptions{1,15}
char
height
%reportoptions{1,16}
char
8.5
%reportoptions{1,17}
char
width
%reportoptions{1,18}
char
7
%reportoptions{1,19}
char
logfile
%reportoptions{1,20}
char

%reportoptions{1,21}
char
visible
%reportoptions{1,22}
char
off
%reportoptions{1,23}
char
maptype
%reportoptions{1,24}
char
Map30_2o
%reportoptions{1,25}
char
DeviationLims
%reportoptions{1,26}
field
3 2
-30 5 
-100 20 
-10 3 
%loadoptions{1,1}
char
output
%loadoptions{1,2}
char
struct
%fitoptions(1,1).fitfun
char
fitbasis21
%fitoptions(1,1).Fs
field
1 1
30 
%fitoptions(1,1).bfcode
char
LGNMloglin
%fitoptions(1,1).nbf
field
1 1
1 
%fitoptions(1,1).bfa
field
1 2
0.44 0.23 
%fitoptions(1,1).UsePrior
field
1 1
1 
%fitoptions(1,1).PriorMethod
char
Fittedplus
%fitoptions(1,1).lagmax
field
1 1
2 
%fitoptions(1,1).nlag2
field
1 1
0 
%fitoptions(1,1).nlagx
field
1 1
0 
%fitoptions(1,1).klagnum
field
1 1
1 
%fitoptions(1,1).TrendOrder
field
1 1
0 
%fitoptions(1,1).HumOrder
field
1 1
0 
%fitoptions(1,1).HumFreq
field
1 1
50 
%fitoptions(1,1).prewhitenCode
char
AR
%fitoptions(1,1).nd
field
1 1
10 
%fitoptions(1,1).ndfree
field
1 1
10 
%fitoptions(1,1).dInitial
field
1 1
1 
%fitoptions(1,1).dReest
field
1 1
0 
%fitoptions(1,1).dupdateMethod
char
Shrink
%fitoptions(1,1).DstartMethod
char
Chol
%fitoptions(1,1).chanweightNum
field
1 1
Inf 
%fitoptions(1,1).chanweightMethod
char
equal
%fitoptions(1,1).DstartExclude
field
1 1
0 
%fitoptions(1,1).excludeOutliers
field
1 1
0 
%fitoptions(1,1).GoodSegmentRestart
field
1 1
1 
%fitoptions(1,1).ExcludedARMethod
char
ExcludedExtended
%fitoptions(1,1).ExcludedCommon
field
1 1
1 
%fitoptions(1,1).RobustMethod
char
none
%fitoptions(1,1).SectionMode
char
none
%fitoptions(1,1).OptimizationMethod
char
GNC
%fitoptions(1,1).Kaufman
field
1 1
1 
%fitoptions(1,1).HessMethod
char
JTJ
%fitoptions(1,1).bMethod
char
channelSeparate
%fitoptions(1,1).BlinkMaskDur
field
1 1
0.5 
%fitoptions(1,1).OutlierRelMedianHiLim
field
1 1
2000 
%fitoptions(1,1).DiffMethod
char
DiffYXT
%fitoptions(1,1).DXsMethod
char
dat
%fitoptions(1,1).Yprecision
char
double
%fitoptions(1,1).dcostTol
field
1 1
0.001 
%fitoptions(1,1).dfMaxAbsTol
field
1 1
0.1 
%fitoptions(1,1).daMaxAbsTol
field
1 1
100 
%fitoptions(1,1).ddMaxAbsTol
field
1 1
100 
%fitoptions(1,1).itermax
field
1 1
100 
%fitoptions(1,1).plotiterEach
field
1 1
0 
%fitoptions(1,1).plotiter
field
1 1
0 
%fitoptions(1,1).plotf
field
1 1
0 
%fitoptions(1,1).plotsig
field
1 1
0 
%fitoptions(1,1).profile
char
off
%fitoptions(1,1).fsuffix
char
_Fs30_DiffYXT_fitbasis17LGNMloglini1Trend0AR10Prior_OneStepGNCfminbnd_EC1_sFixed_PriorFittedplus000_tpPriorSD200
%fitoptions(1,1).fprop(1,1).pl(1,1).vmax
field
0 0
0
%fitoptions(1,1).fprop(1,1).pl(1,1).fcn
char
plvf
%fitoptions(1,1).fprop(1,1).pl(1,1).comp{1,1}
field
1 1
1 
%fitoptions(1,1).fprop(1,1).pl(1,1).comp{1,2}
field
1 1
2 
%fitoptions(1,1).fpostprocess
char

%fitoptions(1,1).fsave
field
1 1
1 
%fitoptions(1,1).force
field
1 1
0 
%fitoptions(1,1).fsuffixExtra
char
_sFixed_PriorFittedplus000_tpPriorSD200
%fitoptions(1,1).keep_bVar
field
1 1
1 
%fitoptions(1,1).renormalize
field
1 1
1 
%fitoptions(1,1).aPriorMeanIncrement
field
1 1
0 
%fitoptions(1,1).iterMethod
char
OneStep
%fitoptions(1,1).aPriorSD
field
1 3
1000 0.2 0 
%fitoptions(1,1).DetMethod
char
none
%fitoptions(1,1).bfnegative
field
1 1
0 
%fitoptions(1,1).StepMethod
char
fminbnd
%fitoptions(1,1).ainitMethod
char
lognormalu
%fitoptions(1,1).dinitMethod
char
none
%fitoptions(1,1).bfAmpCutover
field
1 1
0.1 
%fitoptions(1,1).aPriorMean
field
1 3
0.1 0.44 0.23 
%fitoptions(1,1).afree
field
1 2
1 0 
%fitoptions(1,1).nbftotal
field
1 1
1 
%fitoptions(1,1).output
char
struct
%fitoptions(1,1).prewhitenMethod
char
Segmentfft
%fitoptions(1,1).itermin
field
1 1
2 
