#pragma once
#ifdef REPORTER_EXPORTS
#define REPORTER_API __declspec(dllexport)
#else
#define REPORTER_API __declspec(dllimport)
#endif

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <iostream>
#include <boost/math/tools/polynomial.hpp>
#include <boost/numeric/ublas/storage.hpp>
#include <complex>
#include <valarray>
#include "bbpr.h"
#include <complex>
#include "fft.h"
using namespace boost::numeric::ublas;
using namespace boost::math::tools;
matrix<double> REPORTER_API Max(int A, const matrix<double> B);
double REPORTER_API Max(const matrix<double> B);
matrix<double> REPORTER_API log(const matrix<double> x);
matrix<double> REPORTER_API pow(const matrix<double> x1, double x2);
matrix<double> REPORTER_API exp(const matrix<double> x);
matrix<double> REPORTER_API ceil(const matrix<double> x1);
matrix<double> REPORTER_API floor(const matrix<double> x1);
matrix<double> REPORTER_API mean(const matrix<double> x);
double REPORTER_API mediannotnan(const boost::numeric::ublas::vector<double> x); //Median excluding NaN
matrix<bool> REPORTER_API any(const matrix<bool> x); //extension of the logical OR operator	
matrix<double> REPORTER_API diff(const matrix<double> d);
matrix<double> REPORTER_API find(const matrix<double> d);
matrix<bool> REPORTER_API isnan(const matrix<double> x);
matrix<std::complex<double>> REPORTER_API roots(const matrix<double> x);
matrix<double> REPORTER_API abs(matrix<std::complex<double>> x);
void REPORTER_API fill(matrix<double> *x, const double value);
double interpolate(const matrix<double> xData, const matrix<double> yData, double x);
matrix<double> chol(const matrix<double> A); //Cholesky factorization
matrix<double> speye(int n);
matrix<double> kron(matrix<double> A, matrix<double> B);
matrix<double> reshape(const matrix<double> A, int r, int c);
matrix<std::complex<double>> fft(matrix<std::complex<double>> A, int N);
matrix<std::complex<double>> ifft(matrix<std::complex<double>> A, int N);
matrix<double> REPORTER_API operator +(const matrix<double> x1,const double x2);
matrix<double> REPORTER_API operator +(const double x1, const matrix<double> x2);
matrix<double> REPORTER_API operator -(const matrix<double> x1, const double x2);
matrix<double> REPORTER_API operator *(const matrix<double> x1, const matrix<double> x2);
matrix<double> REPORTER_API operator /(const matrix<double> x1, const matrix<double> x2);
//std::ostream REPORTER_API &operator <<(std::ostream &out, const matrix<double> x);
matrix<bool> REPORTER_API operator ==(const matrix<double> x1, const double x2);
matrix<bool> REPORTER_API operator >(const matrix<double> x1, const double x2);
matrix<bool> REPORTER_API operator <(const matrix<double> x1, const double x2);
matrix<bool> REPORTER_API operator >(const double x1, const matrix<double> x2);
matrix<bool> REPORTER_API operator <(const double x1, const matrix<double> x2);
matrix<bool> REPORTER_API operator >=(const matrix<double> x1, const double x2);
matrix<bool> REPORTER_API operator <=(const matrix<double> x1, const double x2);
matrix<bool> REPORTER_API operator >=(const double x1, const matrix<double> x2);
matrix<bool> REPORTER_API operator <=(const double x1, const matrix<double> x2);
matrix<bool> REPORTER_API operator |(const matrix<bool> x1, const matrix<bool> x2);
matrix<bool> REPORTER_API operator &(const matrix<bool> x1, const matrix<bool> x2);
matrix<bool> REPORTER_API operator !(const matrix<bool> x);
