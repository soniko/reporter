#include "stdafx.h"
#include "FitCreator.h"

FitCreator::FitCreator()
{

}

FitCreator::~FitCreator()
{
}

void FitCreator::fitbasis21()
{
	matrixdata["est.dfMaxAbsTol"]=	matrix<double>(1,1,100);
	matrixdata["est.daMaxAbsTol"] = matrix<double>(1, 1, 100);
	matrixdata["est.dbMaxAbsTol"] = matrix<double>(1, 1, 100);
	matrixdata["est.ddMaxAbsTol"] = matrix<double>(1, 1, 100);
	matrixdata["est.gradnormTol"] = matrix<double>(1, 1, 100);
	matrixdata["est.dcostTol"] = matrix<double>(1, 1, 1e-3);
	matrixdata["est.levlgTol"]= matrix<double>(1, 1, 3);
	stringdata["est.dInitialMethod"] = "estprewhitening";
	double nDfilt = 10;
	double Fs = matrixdata["fitoptions(1,1).Fs"](0, 0);
	double nDfiltfree = 10;
	matrixdata["est.nDfiltfree"] = matrix<double>(1, 1, nDfiltfree);
	bool DstartExclude = false;
	string project = "Pupils";
	int nbftotal = matrixdata["fitoptions(1,1).nbf"](0, 0) * 1;

	matrixdata["r(1,1).var(1,1).Yoffset"] = matrix<double>(1, 1, 0);
	matrixdata["r(1,1).var(1,1).Yscaler"] = matrix<double>(1, 1, 1);

	stringdata["r(1,1).p(1,1).units"] = "um";
	stringdata["r(1,1).p(1,1).unitlabel"] = "\mum";
	matrixdata["r(1,1).var(1,1).ResponseSign"] = matrix<double>(1, 1, 1);
	string Yvar = "Diameter";

	int XnScalar = 1000 / matrixdata["r(1,1).var(1,1).Yscaler"](0, 0);

	if(matrixdata["fitoptions(1,1).Fs"](0,0)< 1 / matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0, 0))
		mfresample(Fs);

	matrixdata["s(1,1).multidim(1,1).p(1,1).regdelay"] = matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"];
	matrixdata["s(1,1).multidim(1,1).p(1,1).pl(1,1).offset"](0,0) = -2;
	matrixdata["est.dt"] = matrixdata["r(1,1).dim(1,1).delta"];	
	double samperstim = round(matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0, 0) / matrixdata["r(1,1).dim(1,1).delta"](0, 0));
	matrixdata["est.samperstim"] = matrix<double>(1, 1, samperstim);
	matrix<double> SegmentLoIndex = 1 + (matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"] - 1)*samperstim;
	matrix<double> SegmentHiIndex = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"]*samperstim;
	double ncond = matrixdata["s(1,1).multidim(1,1).var(1,1).dat"].size2();
	matrixdata["r(1,1).p(1,1).samperstim"] = matrix<double>(1, 1, samperstim);
	matrixdata["r(1,1).p(1,1).SegmentNum"] = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"];
	matrixdata["r(1,1).p(1,1).SegmentLoIndex"] = SegmentLoIndex;
	matrixdata["r(1,1).p(1,1).SegmentHiIndex"] = SegmentHiIndex;
	matrixdata["est.CycleLoIndex"] = 1+(matrixdata["s(1,1).multidim(1,1).p(1,1).CycleLoIndex"] - 1) * samperstim;
	matrixdata["est.CycleHiIndex"] = matrixdata["s(1,1).multidim(1,1).p(1,1).CycleHiIndex"]  * samperstim;
	stringdata["s(1,1).multidim(1,1).p(1,1).xMethod"] = "nonneg";
	matrix<double> xdat1 = matrixdata["s(1,1).multidim(1,1).var(1,1).dat"];
	xdat1 = Max(0, xdat1);
	matrix<double> xdat(xdat1.size1(), xdat1.size2(), 0);
	matrix<double> lagdiff(ncond, 1, 0);
	xdat = xdat1;
	matrix<double> reglag = matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"];

	int nx = ncond * matrixdata["s(1,1).multidim(1,1).p(1,1).nreg"](0, 0); // number of input channels to system
	matrixdata["est.nx"] = matrix<double>(1,1,nx);
	if(samperstim==1)
		matrixdata["est.x.dat"] = xdat;

	int regseparate;
	int isbilinear;
	stringdata["bf.code"] = stringdata["fitoptions(1,1).bfcode"];
	matrixdata["bf.nbf"] = matrixdata["fitoptions(1,1).nbf"];
	matrixdata["bf.dt"] = matrixdata["r(1,1).dim(1,1).delta"];
	stringdata["bf.DiffMethod"] = stringdata["fitoptions(1,1).DiffMethod"];
	stringdata["bf.updateMode"] = "a_plus_da";
	matrixdata["bf.AmpCutover"] = matrixdata["fitoptions(1,1).bfAmpCutover"];
	matrixdata["bf.negative"]= matrixdata["fitoptions(1,1).bfnegative"]; 
	matrixdata["bf.kmin"] = matrix<double>(1, 1,round(0 / matrixdata["r(1,1).dim(1,1).delta"](0,0)));
	matrixdata["bf.kmax"] = matrix<double>(1, 1, round(matrixdata["ndbs(1,1).ndb7(1,1).dim(8,1).slot(1,1).Lagmax"](0,0) / matrixdata["r(1,1).dim(1,1).delta"](0, 0)));
	matrixdata["bf.nlag"] = matrixdata["bf.kmax"] - matrixdata["bf.kmin"] + 1;

	matrixdata["bf.t"] = matrix<double>(matrixdata["bf.kmax"](0, 0)+1, 1, 0);
	matrixdata["bf.nt"] = matrix<double>(1,1, matrixdata["bf.t"].size1());
	matrixdata["bf.a"] = matrixdata["fitoptions(1,1).bfa"];
	matrixdata["bf.afree"] = matrixdata["fitoptions(1,1).afree"];	
	matrixdata["bf.pardim"] = matrix<double>(1, 1, 14);
	for (int i = 0; i < matrixdata["bf.t"].size1(); i++)
		matrixdata["bf.t"](i) = 0 + i * matrixdata["r(1,1).dim(1,1).delta"](0, 0);
	if (stringdata["fitoptions(1,1).bfcode"].find("LGNM") == string::npos)
		regseparate = 0;
	else
		regseparate = 1;
	if (stringdata["fitoptions(1,1).bfcode"].find("bilin") == string::npos || stringdata["fitoptions(1,1).bfcode"].find("bitang") == string::npos)
		isbilinear = 0;
	else
		isbilinear = 1;
	matrixdata["bf.regseparate"] = matrix<double>(1, 1, regseparate);
	matrixdata["bf.isbilinear"] = matrix<double>(1, 1, isbilinear);
	if (isbilinear)
	{


	}
	else
	{
		if (stringdata["fitoptions(1,1).bfcode"].compare("kernel")==0)
		{

		}
		else
		{
			if (regseparate)
			{
				if (matrixdata["fitoptions(1,1).bfa"].size2() == 2)
				{
					matrix<double> bfa(matrixdata["fitoptions(1,1).bfa"].size1(), matrixdata["fitoptions(1,1).bfa"].size2()+1);
					bfa(0, 0) = 1;
					for (int i = 1; i < bfa.size2(); i++)
						bfa(0, i) = matrixdata["fitoptions(1,1).bfa"](0, i-1);
				}
				int nbs = 0; //number of parameters modeling system
				stringdata["fitoptions(1,1).bfcode"] = "lognormalu";
				stringdata["fitoptions(1,1).StepMethod"] = "full";
				stringdata["fitoptions(1,1).OptimizationMethod"] = "ML";
				matrixdata["fitoptions(1,1).UsePrior"](0, 0) = 0;
				matrixdata["fitoptions(1,1).afree"] = matrix<double>(2, 1, 1);
				stringdata["fitoptions(1,1).fsave"] = "off";
				matrixdata["fitoptions(1,1).force"](0, 0) = 1;
				fitbasis21();
				//ending of this code 6045 
			}
			else
			{
				basisfun5();
				matrixdata["est.nbf"] = matrixdata["fitoptions(1,1).nbf"];
				matrixdata["est.nbfTotal"] = matrixdata["fitoptions(1,1).nbftotal"];
				matrixdata["est.nbs"] = matrix<double>(1,1,matrixdata["est.nx"](0,0)* matrixdata["est.nbfTotal"](0,0));
				matrixdata["est.ainit"] = matrixdata["bf.a"];
			}
		}
	}
	matrix<double> Dfilt;
	if (stringdata["fitoptions(1, 1).prewhitenCode"].compare("AR4v1") == 0)
	{
		Dfilt.resize(5, 1);
		Dfilt(0) = 1.0;
		Dfilt(1) = -1.1157;
		Dfilt(2) = -0.0108;
		Dfilt(3) = 0.0079;
		Dfilt(4) = 0.1215;
	}
	else
	{
		if (stringdata["fitoptions(1,1).prewhitenCode"].compare("AR") == 0)
		{
			double nd = matrixdata["fitoptions(1,1).nd"](0, 0);
			Dfilt.resize(nd + 1, 1, 0);
			Dfilt(0) = 1;
		}
		else
		{
			if (stringdata["fitoptions(1,1).prewhitenCode"].compare("ART") == 0)
			{
				double nd = matrixdata["fitoptions(1,1).nd"](0, 0);
				Dfilt.resize(nd + 1, 1, 0);
				Dfilt(0) = 1;
			}
			else
			{
				if (stringdata["fitoptions(1,1).prewhitenCode"].compare("AR1v1") == 0)
				{
					Dfilt.resize(2, 1);
					Dfilt(0) = 1;
					Dfilt(1) = -0.9961;
				}
				else
				{
					if (stringdata["fitoptions(1,1).prewhitenCode"].compare("AR1v2") == 0)
					{
						Dfilt.resize(2, 1);
						Dfilt(0) = 1;
						Dfilt(1) = -0.99;
					}
					else
					{
						if (stringdata["fitoptions(1,1).prewhitenCode"].compare("AR1p9") == 0)
						{
							Dfilt.resize(2, 1);
							Dfilt(0) = 1;
							Dfilt(1) = -0.9;
						}
						else
						{
							if (stringdata["fitoptions(1,1).prewhitenCode"].compare("AR2v1") == 0)
							{
								Dfilt.resize(3, 1);
								Dfilt(0) = 1;
								Dfilt(1) = -1.5284;
								Dfilt(2) = 0.5340;
							}
							else
							{
								Dfilt.resize(1, 1);
								Dfilt(0) = 1;
							}
						}
					}
				}
			}
		}

	}

	Dfilt.resize(Dfilt.size1(), 2);
	for (int i = 0; i < Dfilt.size1(); i++)
		Dfilt(i, 1) = Dfilt(i, 0);
	matrixdata["est.Dfilt"] = Dfilt;
	matrixdata["est.nd"] = matrix<double>(1,1,Dfilt.size1() - 1);
	matrixdata["est.nstart"] = matrixdata["est.nd"]; // total segment start length
	matrix <double> dat = matrixdata["r(1,1).var(1,1).dat"];

	matrix <double> missing (dat.size1(),dat.size2(),0);

	for (int i = 0; i < dat.size1(); i++)
	{
		for (int j = 0; j < dat.size2(); j++)
			if (isnan(dat(i, j)))
			{
				missing(i, j) = 1;
			}		
			else
				missing(i, j) = 0;
	}

	matrixdata["r(1,1).var(1,1).Missing"] = missing;
	matrix <double> Excluded = missing > 0;
	matrixdata["r(1,1).var(1,1).Excluded"] =Excluded;
	matrixdata["r(1,1).var(1,1).ExcludedNum"] = sum(Excluded);
	if (matrixdata["fitoptions(1,1).ExcludedCommon"](0, 0) == 1)
	{
		matrix<double> d = any(matrixdata["r(1,1).var(1,1).Excluded"]);
		for (int i = 0; i < d.size1(); i++)
		{
			Excluded(i, 0) = d(i, 0);
			Excluded(i, 1) = d(i, 0);
		}
		matrixdata["r(1,1).var(1,1).Excluded"] = Excluded;
		matrixdata["r(1,1).var(1,1).ExcludedNum"] = sum(Excluded);
	}
	matrixdata["r(1,1).var(1,1).datraw"] = matrixdata["r(1,1).var(1,1).dat"];
	stringdata["r(1,1).var(1,1).segmentMethod"] = "extrapconst";
	matrixdata["r(1,1).var(1,1).OutlierDevThreshold"] = matrix<double>(1,1,0);
	matrixdata["r(1,1).var(1,1).OutlierRelMedianHiLim"] = matrixdata["fitoptions(1,1).OutlierRelMedianHiLim"];

	ypreprocess7();

	matrixdata["f.var.datmean"] = mean(matrixdata["r(1,1).var(1,1).dat"]) + matrixdata["r(1,1).var(1,1).Yoffset"](0,0) / matrixdata["r(1,1).var(1,1).Yscaler"](0,0);
	if (matrixdata["fitoptions(1,1).BlinkMaskDur"](0, 0) > 0, 5)
	{
		stringdata["r(1,1).p(1,1).BlinkMethod"] = "Missingdur";
		matrixdata["r(1,1).p(1,1).BlinkMaskDur"] = matrix<double>(1, 1, 0.5);
		matrixdata["r(1,1).p(1,1).BlinkDurMin"] = matrix<double>(1, 1, 0.2);	
		matrixdata["r(1,1).p(1,1).BlinkWaveDur"] = matrix<double>(1, 1, 2);
		//6297 matlab
		//pupilblinkfind
		missing = matrixdata["r(1,1).var(1,1).Missing"];

		matrix<double> missingcol(matrixdata["r(1,1).var(1,1).Missing"].size1(), 1, 0);
		for (int i = 0; i < missingcol.size1(); i++)
		{
			missingcol(i, 0) = matrixdata["r(1,1).var(1,1).Missing"](i, 0) && matrixdata["r(1,1).var(1,1).Missing"](i, 1);
		}

		matrix<double> MissingSegmentLo = diffbackward(missingcol)==1;
		for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentLoIndex"].size2(); i++)
		{
			MissingSegmentLo(matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0,i)-1,0) = missing(matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0,i)-1,0) == 1;
		}
		matrix<double> MissingSegmentHi = diffforward(missingcol) == -1;
		for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentHiIndex"].size2(); i++)
		{		
			MissingSegmentHi(matrixdata["r(1,1).p(1,1).SegmentHiIndex"](0,i)-1,0) = missing(matrixdata["r(1,1).p(1,1).SegmentHiIndex"](0,i)-1,0) == 1;
		}
		std::vector<int> LoIndexV, HiIndexV;
		for (int i = 0; i < MissingSegmentLo.size1(); i++)
		{
			if (MissingSegmentLo(i,0) != 0)
				LoIndexV.push_back(i);
		}
		for (int i = 0; i < MissingSegmentHi.size1(); i++)
		{
			if (MissingSegmentHi(i,0) != 0)
				HiIndexV.push_back(i);
		}
		matrix<double> HiIndex(HiIndexV.size(),1), LoIndex(LoIndexV.size(),1);
		for (int i=0; i<HiIndexV.size(); i++)
		{
			HiIndex.insert_element(i,0, HiIndexV[i]);
		}
		for (int i = 0; i < LoIndexV.size(); i++)
		{
			LoIndex.insert_element(i,0,LoIndexV[i]);
		}
		matrix<double> Length = HiIndex - LoIndex + 1;
		matrix<double> tstart = (LoIndex) * matrixdata["r(1,1).dim(1,1).delta"](0, 0);
		matrix<double> dur =(floor(Length * matrixdata["r(1,1).dim(1,1).delta"](0, 0)*1000))/1000;
		matrix<double> tinrep = (LoIndex) * matrixdata["r(1,1).dim(1,1).delta"](0, 0);
		matrix<double> Blink = dur > matrixdata["r(1,1).p(1,1).BlinkDurMin"](0,0) | ismember(LoIndex, matrixdata["r(1,1).p(1,1).SegmentLoIndex"]);
		
		std::vector<int> bstart;
		for (int i = 0; i < Blink.size1(); i++)
			if (Blink(i, 0) == 1)
				bstart.push_back(HiIndex(i, 0));
		matrix<double> BlinkStart(bstart.size(), 1,0);
		for (int i = 0; i < bstart.size(); i++)
		{
			BlinkStart(i, 0) = bstart[i];
		}
		int BlinkNum = bstart.size();

		boost::numeric::ublas::vector<int> Blinkfilt(round(matrixdata["r(1,1).p(1,1).BlinkMaskDur"](0,0) / matrixdata["r(1,1).dim(1,1).delta"](0, 0))+1, 1);
		Blinkfilt(0) = 0;
		matrix<double> b(matrixdata["r(1,1).var(1,1).dat"].size1(), 1, 0);
		for (int pos :bstart)
		{
			b(pos) = 1;
		}
		int len;
		for (int seg = 0; seg < matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0); seg++)
		{	
			len=matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"](0,seg) - matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0,seg)+1;
			matrix<double> x(len, 1);
			for (int iter = 0; iter < len; iter++)
			{
				x(iter, 0) = b(matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0,seg) - 1 + iter, 0);
			}
			matrix<double> a(Blinkfilt.size(), 1, 0);
			a(0) = 1;
			matrix<double> val = filter(Blinkfilt, a, x);

			//for (int i = 0; i < val.size1(); i++)
			//{
			//	if (val(i, 0) != 0)
			//		cout << i + matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, seg) << " " << val(i, 0) << endl;
			//}
			for (int iter = 0; iter < len; iter++)
			{
				b(matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0,seg) - 1+iter, 0) = val(iter, 0);
			}
		}
		matrixdata["r(1,1).var(1,1).BlinkMask"] = b;
		matrix<double>oldnum = matrixdata["r(1,1).var(1,1).ExcludedNum"];
		for (int i = 0; i < matrixdata["r(1,1).var(1,1).Excluded"].size1(); i++)
			for (int j = 0; j < matrixdata["r(1,1).var(1,1).Excluded"].size2(); j++)
				if (b(i, 0) == 1)
					matrixdata["r(1,1).var(1,1).Excluded"](i, j) = 1;

		matrixdata["r(1,1).var(1,1).ExcludedNum"] = sum(matrixdata["r(1,1).var(1,1).Excluded"]);
		matrixdata["r(1,1).var(1,1).BlinkMaskNum"] = sum(matrixdata["r(1,1).var(1,1).Excluded"]) - oldnum;

		matrixdata["r(1,1).p(1,1).MissingSegments.var.LoIndex"]=LoIndex;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.HiIndex"]=HiIndex;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.Length"]=Length;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.tstart"]=tstart;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.dur"]=dur;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.tinrep"]=tinrep;
		matrixdata["r(1,1).p(1,1).MissingSegments.var.Blink"]=Blink;
		matrixdata["options.BlinkStart"]= BlinkStart;
		matrixdata["optionsr.BlinkNum"] = matrix<double>(1, 1, BlinkNum);
	}

	//matlab 6313
	//estxnmake func
	estxnmake();

	matrixdata["r(1,1).p(1,1).bnInitial"] = matrix<double>(matrixdata["est.nbn"](0, 0), 2, 0);
	string DiffMethod = stringdata["fitoptions(1,1).DiffMethod"];
	boost::algorithm::to_lower(DiffMethod);
	if (DiffMethod.compare("diffnone") == 0)
	{
		matrixdata["r(1,1).p(1,1).Aextra"] = matrix<double>(1, 1, 1);
		matrixdata["r(1,1).p(1,1).Ddiff"] = matrix<double>(1, 1, 1);
	}
	else if (DiffMethod.compare("diffy")==0)
	{
		matrixdata["r(1,1).p(1,1).Aextra"] = matrix<double>(2, 1, 1);
		matrixdata["r(1,1).p(1,1).Aextra"](1, 0) = -1;
		matrixdata["r(1,1).p(1,1).Ddiff"] = matrix<double>(1, 1, 1);
	}
	else if (DiffMethod.compare("diff2y") == 0)
	{

	}
	else if (DiffMethod.compare("diffyx") == 0)
	{
		matrixdata["r(1,1).p(1,1).Aextra"] = matrix<double>(1, 1, 1);
		matrixdata["r(1,1).p(1,1).Ddiff"] = matrix<double>(2, 1, 1);
		matrixdata["r(1,1).p(1,1).Ddiff"](1, 0) = -1;
	}
	else if (DiffMethod.compare("diffyxt") == 0)
	{
		matrixdata["r(1,1).p(1,1).Aextra"] = matrix<double>(1, 1, 1); 
		matrixdata["r(1,1).p(1,1).Ddiff"] = matrix<double>(2, 1, 1);
		matrixdata["r(1,1).p(1,1).Ddiff"](1, 0) = -1;
		matrixdata["r(1,1).p(1,1).datFirstPoint"] = matrix<double>(matrixdata["r(1,1).p(1,1).SegmentLoIndex"].size1(), 2, 1);
		for(int i=0; i<matrixdata["r(1,1).p(1,1).datFirstPoint"].size1(); i++)
			for (int j = 0; j < 2; j++)
			{
				matrixdata["r(1,1).p(1,1).datFirstPoint"](i, j) = matrixdata["r(1,1).var(1,1).dat"](matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0,i)-1, j);
			}

		matrixdata["r(1,1).var(1,1).dat"] = matrixdata["r(1,1).var(1,1).dat"] - delay(matrixdata["r(1,1).var(1,1).dat"]);
		for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentLoIndex"].size2(); i++)
			for (int j = 0; j <2; j++)
			{
				matrixdata["r(1,1).var(1,1).dat"](matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0, i) - 1, j) = 0;
			}

		boost::numeric::ublas::vector<int> b(2,1);
		matrix<double> a(2,1,0);
		a(0,0) = 1;
		matrix<double> col1(matrixdata["r(1,1).var(1,1).Excluded"].size1(), 1);
		for (int j = 0; j < matrixdata["r(1,1).var(1,1).Excluded"].size2(); j++)
		{
			for (int i = 0; i < col1.size1(); i++)
			{
				col1(i, 0) = matrixdata["r(1,1).var(1,1).Excluded"](i, j);
			}
			matrix<double> tempcolumn = filter(b, a, col1) > 0;
			for (int i = 0; i < col1.size1(); i++)
			{
				matrixdata["r(1,1).var(1,1).Excluded"](i, j) = tempcolumn(i, 0);
			}
		}
		for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentLoIndex"].size2(); i++)
			for (int j = 0; j < 2; j++)
			{
				matrixdata["r(1,1).var(1,1).Excluded"](matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0, i) - 1, j) = 1;
			}
		matrixdata["r(1,1).var(1,1).ExcludedNum"] = sum(matrixdata["r(1,1).var(1,1).Excluded"]);
		matrixdata["est.x.dat"] = matrixdata["est.x.dat"] - delay(matrixdata["est.x.dat"]); //must be delaycy but in this case do exactly same thing as delay
		matrixdata["f.var.Const"] = matrixdata["f.var.datmean"];
	}	
	matrixdata["fitoptions(1,1).chanweightNum"](0, 0) = min(matrixdata["fitoptions(1,1).chanweightNum"](0, 0), 2);  //number of output channels to include in optimization
	matrixdata["est.klag"] = matrix<double>(1, 1, 1);
	matrixdata["est.x.xcymat.datcy"] = matrixdata["est.x.dat"];

	matrixdata["est.x.xcymat.lagcy"] = matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"];
	matrixdata["est.x.xcymat.CycleNum"] = matrixdata["s(1,1).multidim(1,1).p(1,1).CycleNum"];
	matrixdata["est.x.xcymat.CycleLoIndex"] = matrixdata["est.CycleLoIndex"];
	matrixdata["est.x.xcymat.CycleHiIndex"] = matrixdata["est.CycleHiIndex"];
	stringdata["f.p.units"] = stringdata["r(1,1).p(1,1).units"];
	stringdata["f.p.unitlabel"] = stringdata["r(1,1).p(1,1).unitlabel"];
	matrixdata["f.dim.delta"] = matrixdata["r(1,1).dim(1,1).delta"];
	//Store for previous iteration values
	matrixdata["est.old.cost"] = matrix<double>(1,1,INFINITY);
	matrixdata["est.old.bfull"] = matrix<double>(1, 1, INFINITY);
	matrixdata["est.old.td"] = matrix<double>(1, 1, INFINITY);
	matrixdata["est.old.bf.a"] = matrixdata["bf.a"];
	fitcalc();

}
	

void FitCreator::mfresample(double Fs)
{ 
	double respfreq = 1/ matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0, 0);
	int fac = round(respfreq / Fs);
	if ((respfreq - fac * Fs) > 1)
		return; //  error is more than 1 Hz
	matrixdata["r(1,1).var(1,1).dat"] =  resample(matrixdata["r(1,1).var(1,1).dat"], fac);
	matrixdata["r(1,1).var(1,1).brightness"] = resample(matrixdata["r(1,1).var(1,1).brightness"], fac);
	matrixdata["r(1,1).var(1,1).PosX"] = resample(matrixdata["r(1,1).var(1,1).PosX"], fac);
	matrixdata["r(1,1).var(1,1).PosY"] = resample(matrixdata["r(1,1).var(1,1).PosY"], fac);
	matrixdata["r(1,1).var(1,1).MissingNum"] = resample(matrixdata["r(1,1).var(1,1).MissingNum"], fac);
	matrixdata["r(1,1).var(1,1).Yoffset"] = matrix<double>(1, 1, 0);
	matrixdata["r(1,1).var(1,1).Yscaler"] = matrix<double>(1, 1, 1);
	matrixdata["r(1,1).var(1,1).ResponseSign"] = matrix<double>(1, 1, 1);
	matrixdata["r(1,1).var(1,1).Excluded"] = matrix<double>(1, 1, 0);
	//matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"] = matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"]*fac;
	matrixdata["r(1,1).dim(1,1).delta"] = matrix<double>(1,1, matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0,0) * fac);

	// resample stimulus

	double stimfreq = 1 / matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0, 0);
	if (stimfreq > Fs)
	{
		int sfac = round(stimfreq / Fs);
		if ((int)(matrixdata["s(1,1).multidim(1,1).p(1,1).sparsefac"](0,0)) % sfac != 0)
			return;	
		if (matrixdata["s(1,1).multidim(1,1).p(1,1).sparsefac"](0, 0) != matrixdata["s(1,1).multidim(1,1).p(1,1).fdot"](0, 0))
			return;	
		string vnames[9] = {"runlen","preludelen","reglag",	"regcondlag","reglagmin","reglagmax","sparsefac","rightdelay","fdot"};
		for (int i = 0; i < 9; i++)
		{
			matrixdata["s(1,1).multidim(1,1).p(1,1)." + vnames[i]] = matrixdata["s(1,1).multidim(1,1).p(1,1)." + vnames[i]] / 2;
		}
	/*	string varnames[33] = { "dat","cdat","condlag","meanfreqrealized" ,"taskdat" ,"fixationdat" ,"ring" ,"sector" ,"elo" ,"ehi" ,"alo" ,"ahi" ,"xmin" ,"xmax" ,"ymin" ,"ymax" ,"xcentre" ,"ycentre" ,"emid" ,"amid" ,"xmid" ,"ymid" ,"regionfliplr" ,"regionflipud" ,"regarea" ,"ixmin" ,"ixmax" ,"iymin" ,"iymax" ,"destwidth" ,"destheight" ,"xcentrepix" ,"ycentrepix" };*/

		matrixdata["s(1,1).multidim(1,1).var(1,1).dat"] = resample(matrixdata["s(1,1).multidim(1,1).var(1,1).dat"], sfac);
		matrixdata["s(1,1).multidim(1,1).var(1,1).cdat"] = resample(matrixdata["s(1,1).multidim(1,1).var(1,1).cdat"], sfac);
		matrixdata["s(1,1).multidim(1,1).var(1,1).taskdat"] = resample(matrixdata["s(1,1).multidim(1,1).var(1,1).taskdat"], sfac);
		matrixdata["s(1,1).multidim(1,1).var(1,1).fixationdat"] = resample(matrixdata["s(1,1).multidim(1,1).var(1,1).fixationdat"], sfac);
		matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"] = matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"]*sfac;
		matrixdata["s(1,1).multidim(1,1).dim(1,1).size"] = matrix<double> (1,1, matrixdata["s(1,1).multidim(1,1).var(1,1).dat"].size1());
		matrixdata["s(1,1).multidim(1,1).p(1,1).regdelay"] = matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"];
		matrixdata["s(1,1).multidim(1,1).p(1,1).fdot"] = matrixdata["s(1,1).multidim(1,1).p(1,1).sparsefac"];

		matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"] = 1 + ceil((matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"]-1)/sfac);
		matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"] = 1 + floor((matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"]-1)/sfac);
		matrixdata["s(1,1).multidim(1,1).p(1,1).CycleLoIndex"] = 1 + ceil((matrixdata["s(1,1).multidim(1,1).p(1,1).CycleLoIndex"] - 1) / sfac);
		matrixdata["s(1,1).multidim(1,1).p(1,1).CycleHiIndex"] = 1 + floor((matrixdata["s(1,1).multidim(1,1).p(1,1).CycleHiIndex"] - 1) / sfac);
	}
}

matrix <double> FitCreator::resample(matrix <double> m, int fac)
{
	if (m.size1() > 0)
	{
		matrix <double> output(m.size1() / fac, m.size2());
		if (output.size1() < 1)
			return m;
		int iter = 0;
		for (int i = 0; i < m.size1(); i = i + fac)
		{
			output(iter, 0) = m(i, 0);
			output(iter, 1) = m(i, 1);
			iter++;
		}
		return output;
	}
	else
	{
		return 	m;
	}

}

void FitCreator::basisfun5()
{
	matrix<double> t = matrixdata["bf.t"];
	double dt = matrixdata["bf.t"](1) - matrixdata["bf.t"](0);

	matrix<double> f;
	matrix<double> J;
	matrixdata["bf.aRow"] = matrixdata["bf.a"];
	matrixdata["bf.na"] = matrix<double>(1,1, matrixdata["bf.aRow"].size2());
	matrixdata["bf.rankdef"] = matrix<double>(1, 1, 0);
	matrixdata["bf.nconstraint"] = matrix<double>(1, 1, 0);

	if (stringdata["bf.code"].compare("lognormalu") == 0)
	{
		int np1 = 2;
		matrixdata["bf.a"] = Max(0.01, matrixdata["bf.a"]);
		double tp = matrixdata["bf.a"](0, 0);
		double s = matrixdata["bf.a"](0, 1);
		int nlgn = 1;
		matrix<double> tn = t / tp;
		std::vector<int> pos;
		for (int i = 0; i < tn.size1(); i++)
		{
			if (tn(i) <= 0)
			{
				tn(i) = 1;
				pos.push_back(i);
			}
		}

		matrix<double> logtn = log(tn);
		f = exp(-0.5*((pow(logtn - pow(s, 2), 2)) /( s * s)) + (-log(s)) + (-log(Max(1e-6, t))) + (-0.5*log(2 * boost::math::constants::pi<double>())));

		for (int val:pos)
		{
			f(val) = 0;
		}
		matrixdata["bf.PeakValue"] = matrix<double>(1, 1, exp(-0.5*pow(s, 2)) / (s * tp * sqrt(2 * boost::math::constants::pi<double>())));
		matrixdata["bf.Peaktime"] = matrix<double>(1, 1, tp);

		matrix<double> f_tp = f * ((logtn / (pow(s, 2)*tp)) - (1 / tp));

		matrix<double> x = ((logtn * logtn) / pow(s, 3));
		double y = 1 / s + s;
		matrix<double> z = x - y;
		matrix<double> ans = f * z;
		matrix<double> f_s = ans;
		//above code do exatly this operators but with low calculation accuracy WTF
		//matrix<double> f_s = f * (((logtn*logtn) / pow(s, 3)) - 1 / s + s);
		J.resize(f_tp.size1(), 2, 0);
		for (int i = 0; i < J.size1(); i++)
		{
			J(i, 0) = f_tp(i, 0);
			J(i, 1) = f_s(i, 0);
		}

		matrixdata["bf.c"] = matrix<double>(1, 1, 1);
		matrixdata["bf.tp"] = matrix<double>(1, 1, tp);
		matrixdata["bf.s"] = matrix<double>(1, 1, s);
		matrixdata["bf.w"] = matrix<double>(1, 1, (2*tp*sinh(sqrt (2*log(2))*s)));

	}
	else
	{

	}
	string DiffMethod = stringdata["bf.DiffMethod"];
	boost::algorithm::to_lower(DiffMethod);
	//noone of them from optfile
	if (DiffMethod.compare("diffy") == 0)
	{

	}
	else
	{
		if (DiffMethod.compare("diff2y") == 0)
		{

		}
	}
	matrixdata["bf.dat"] = f;
	matrixdata["bf.datscaler"] = matrix<double>(1,1,1);
	matrixdata["bf.datraw"] = f;
	matrixdata["bf.J"] = J;
}

bool FitCreator::Create(string respFile, string stimFile, string fitFile)
{
RespFile = respFile;
StimFile = stimFile;
FitFile = fitFile;

readtxt("ndbs.txt");
readtxt("optionsfile.txt");
readtxt("massdata.txt");
readtxt(respFile);

const char* path = "C:\\MASSs";
boost::filesystem::path dir(path);
boost::filesystem::create_directory(dir);

pupilread();
fitbasis21();

return false;
}

void FitCreator::pupilread()
{
	cmatch res;
	regex r("([[:digit:]]+)_20");
	string MRN;
	string date;
	if (regex_search(RespFile.c_str(), res, r))
		MRN == res[1];
	else
		MRN = "unknown";
	r = "_(20[[:digit:]]+T[[:digit:]]+)";
	if (regex_search(RespFile.c_str(), res, r))
		date == res[1];
	else
		date = " ";

	supdate();
	stringdata["s(1,1).multidim(1,1).p(1,1).loadfile"] = StimFile;
	stringdata["s(1,1).multidim(1,1).dim(2,1).name"] = "component";
	stringdata["s(1,1).multidim(1,1).dim(2,1).slot(1,1).name"] = "LeftStim";
	stringdata["s(1,1).multidim(1,1).dim(2,1).slot(2,1).name"] = "RightStim";
	int versionNumber = matrixdata["s(1,1).multidim(1,1).p(1,1).versionNumber"](0, 0);	
	matrix<double> rdat;
	string channames[4] = { "LeftPupil", "RightPupil", "LeftBrightness", "RightBrightness" };
	switch (versionNumber)
	{
	case 1002:
		matrixdata["r(1,1).p(1,1).brightnessscaler"] = matrix<double>(1, 1, 10);
		matrixdata["r(1,1).p(1,1).pixelscaler"] = matrix<double>(1, 1, 1e-4);
		for (int pupil = 1; pupil <= 2; pupil++)
		{

		}
		break;
	case 1003:
		matrixdata["r(1,1).p(1,1).brightnessscaler"] = matrix<double>(1, 1, 10);
		matrixdata["r(1,1).p(1,1).pixelscaler"] = matrix<double>(1, 1, 1e-4);
		matrix<double> framewanted;
		int length = 0;

		for (int iseg = 0; iseg < matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0); iseg++)
		{
			length = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentRecordFrameHi"](0, iseg) - matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentRecordFrameLo"](0, iseg)+1;
			framewanted.resize(1, framewanted.size2() + length);
			for (int i = 0; i < length; i++)
			{
				framewanted(0, framewanted.size2() - length + i) = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentRecordFrameLo"](0, iseg) + i;
			}
		}

		stringdata["r(1,1).p(1,1).RecordingInfo(1,1).SchemaVersion"] = "2003";	
		matrix<double> dat = readfile(StimFile);
		matrix_column<matrix<double>> col (dat, 0);
		matrixdata["r(1,1).p(1,1).sframeMax"] = matrix<double>(1, 1, (*std::max_element(col.begin(), col.end())));
		matrixdata["r(1,1).p(1,1).sframewantedMax"] = matrix<double>(1, 1, (*std::max_element(framewanted.cbegin1(), framewanted.cend1())));
		if (matrixdata["r(1,1).p(1,1).sframeMax"](0, 0) < matrixdata["r(1,1).p(1,1).sframewantedMax"](0, 0))
			return;
	
		rdat.resize(framewanted.size2(), dat.size2());
		for (int i = 0; i < framewanted.size2(); i++)
		{
			matrix_row<matrix<double> > mr1(dat, framewanted(0, i)-1);
			matrix_row<matrix<double> > mr2(rdat, i);
			mr2 = mr1;
		}

		if (stringdata["r(1,1).p(1,1).RecordingInfo(1,1).SchemaVersion"] == "2003")
		{
			matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0) = 10;
			for (int i = 0; i < rdat.size1(); i++)
			{
				for (int j = 0; j < 2; j++)
				{
					matrixdata["r(1,1).var(1,1).dat"](i, j) = round(rdat(i, j)*1e6);				//r.var.dat
				}
			}
				
			for (int i = 0; i < rdat.size1(); i++)
				for (int j = 0; j < 2; j++)
				{
					matrixdata["r(1,1).var(1,1).brightness"](i, j) = rdat(i, j + 2)*matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0);				//r.var.brightness

				}
		}
		else
			if (stringdata["r(1,1).p(1,1).RecordingInfo(1,1).SchemaVersion"] == "2004")
			{
				matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0) = 10;
				for (int i = 0; i < rdat.size1(); i++)
					for (int j = 0; j < 2; j++)
					{
						matrixdata["r(1,1).var(1,1).dat"](i, j) = round(rdat(i, j));				//r.var.dat
					}
				for (int i = 0; i < rdat.size1(); i++)
					for (int j = 0; j < 2; j++)
					{
						matrixdata["r(1,1).var(1,1).brightness"](i, j) = rdat(i, j + 2)*matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0);				//r.var.brightness

					}
				
			}
			else
				if (stringdata["r(1,1).p(1,1).RecordingInfo(1,1).SchemaVersion"] == "2005")
				{
					matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0) = 1e-3;
					matrixdata["r(1,1).var(1,1).dat"] = matrix<double>(rdat.size1(), 2);
					matrixdata["r(1,1).var(1,1).brightness"] = matrix<double>(rdat.size1(), 2);
					matrixdata["r(1,1).var(1,1).PosX"] = matrix<double>(rdat.size1(), 2);
					matrixdata["r(1,1).var(1,1).PosY"] = matrix<double>(rdat.size1(), 2);
					for (int i = 0; i < rdat.size1(); i++)
						for (int j = 0; j < 2; j++)
						{
							matrixdata["r(1,1).var(1,1).dat"](i, j) = round(rdat(i, j+1));				//r.var.dat
							
						}
					for (int i = 0; i < rdat.size1(); i++)
						for (int j = 0; j <2; j++)
						{
							matrixdata["r(1,1).var(1,1).brightness"](i, j) = rdat(i, j+2)*matrixdata["r(1,1).p(1,1).brightnessscaler"](0, 0);				//r.var.brightness

						}
					for (int i = 0; i < rdat.size1(); i++)
						{
							matrixdata["r(1,1).var(1,1).PosX"](i, 0) = rdat(i, 5);				//r.var.PosY
							matrixdata["r(1,1).var(1,1).PosX"](i, 1) = rdat(i, 7);			//r.var.PosY
						}
					for (int i = 0; i < rdat.size1(); i++)
						{
							matrixdata["r(1,1).var(1,1).PosY"](i, 0) = rdat(i, 6);			//r.var.PosX
							matrixdata["r(1,1).var(1,1).PosY"](i, 1) = rdat(i, 8);				//r.var.PosX
						}
				}
				else
					return;

		break;
	}
	stringdata["r(1,1).p(1,1).datatype"] = "pupils";
	stringdata["r(1,1).p(1,1).units"] = "um";
	stringdata["r(1,1).p(1,1).unitlabel"] = "\mum";
	stringdata["r(1,1).dim(2,1).name"] = "channel";
	stringdata["r(1,1).dim(2,1).slot(1,1).name(1,1)"] = channames[0];
	stringdata["r(1,1).dim(2,1).slot(1,1).name(1,2)"] = channames[1];
	matrix <double> sum(1,2,0);
	for (int i = 0; i < rdat.size1(); i++)
	{
		if (isnan(rdat(i, 1)))
			sum(0)++;
		if (isnan(rdat(i, 2)))
			sum(1)++;
	}
	matrixdata["r(1,1).var(1,1).MissingNum"] = sum;
	stringdata["r(1,1).p(1,1).pl(1,1).fcn"] = ""; 
	matrixdata["r(1,1).p(1,1).pl(1,1).offset"] = matrix <double>(1, 1, 0);
	matrixdata["r(1,1).p(1,1).pl(1,1).tsegment"] = (trans(matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"]) - 1)  * matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"](0, 0);

}

void FitCreator::fitcalc()
{
	int nt = matrixdata["r(1,1).var(1,1).dat"].size1();
	int nchan = matrixdata["r(1,1).var(1,1).dat"].size2();
	matrixdata["est.nfft"] = matrix<double>(1,1,nt);
	matrixdata["est.nlag"] = matrix<double>(1, 1, min(1 + round(matrixdata["fitoptions(1,1).lagmax"](0,0) / matrixdata["est.dt"](0,0)), matrixdata["bf.dat"].size1()));
	matrixdata["f.var.chanweight"] = matrix<double>(1, nchan,1);
	matrixdata["f.var.chanweight"] = matrix<double>(1, nchan, (double)NAN);
	matrixdata["est.GoodSegmentLengthMin"] = matrixdata["est.nstart"];
	ygoodsegfind20();
	matrixdata["est.ncap"] = sum(!matrixdata["r(1,1).var(1,1).Excluded"]);
	if (stringdata["fitoptions(1,1).iterMethod"].compare("OneStep") == 0)
	{
		OneStep();
	}
	else if (stringdata["fitoptions(1,1).iterMethod"].compare("MultiStage") == 0)
	{
		//multistage method never call by optionsfile
	}

}

//next discribe from matlab
//These variates are sparse logical marking good segments derived from y.var.Excluded logical and SegmentLoIndex, SegmentHiIndex
void FitCreator::ygoodsegfind20()
{
	matrixdata["r(1,1).var(1,1).GoodSegmentLo"] = diffbackward(matrixdata["r(1,1).var(1,1).Excluded"]) == -1;
	for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentLoIndex"].size2(); i++)
	{
		for (int j = 0; j < matrixdata["r(1,1).var(1,1).GoodSegmentLo"].size2(); j++)
		{
			matrixdata["r(1,1).var(1,1).GoodSegmentLo"](matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0, i)-1, j) = !(matrixdata["r(1,1).var(1,1).Excluded"](matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0, i) - 1, j));
		}
	}

	matrixdata["r(1,1).var(1,1).GoodSegmentHi"] = diffforward(matrixdata["r(1,1).var(1,1).Excluded"]) == 1;
	for (int i = 0; i < matrixdata["r(1,1).p(1,1).SegmentHiIndex"].size2(); i++)
	{
		for (int j = 0; j < matrixdata["r(1,1).var(1,1).GoodSegmentHi"].size2(); j++)
			matrixdata["r(1,1).var(1,1).GoodSegmentHi"](matrixdata["r(1,1).p(1,1).SegmentHiIndex"](0, i)-1, j) = !(matrixdata["r(1,1).var(1,1).Excluded"](matrixdata["r(1,1).p(1,1).SegmentHiIndex"](0, i) - 1, j));
	} 

	matrix <double> GoodSegmentLoLinearIndex = find(matrixdata["r(1,1).var(1,1).GoodSegmentLo"]);
	matrix <double> GoodSegmentHiLinearIndex = find(matrixdata["r(1,1).var(1,1).GoodSegmentHi"]);
	matrixdata["est.GoodSegmentLengthMin"](0,0) = max(matrixdata["est.GoodSegmentLengthMin"](0,0), matrixdata["est.nstart"](0,0));
	matrix<double> seglen = GoodSegmentHiLinearIndex - GoodSegmentLoLinearIndex+1;
	matrix<double> shortseg = find(seglen < matrixdata["est.GoodSegmentLengthMin"](0, 0));
	matrixdata["est.ShortSegmentNum"] = matrix<double>(1,1,shortseg.size1());
	if (shortseg.size1() != 0)
	{
		//drop sparse flags
		for (int i = 0; i < shortseg.size1(); i++)
		{
			int posLo = GoodSegmentLoLinearIndex(shortseg(i, 0));	
			int posHi = GoodSegmentHiLinearIndex(shortseg(i, 0));
			if (posLo < matrixdata["r(1,1).var(1,1).GoodSegmentLo"].size1())			
				matrixdata["r(1,1).var(1,1).GoodSegmentLo"](posLo, 0) = false;			
			else					
				matrixdata["r(1,1).var(1,1).GoodSegmentLo"](posLo-matrixdata["r(1,1).var(1,1).GoodSegmentLo"].size1(), 1) = false;		
				
			if (posHi < matrixdata["r(1,1).var(1,1).GoodSegmentHi"].size1())					
				matrixdata["r(1,1).var(1,1).GoodSegmentHi"](posHi, 0) = false;		
			else				
				matrixdata["r(1,1).var(1,1).GoodSegmentHi"](posHi - matrixdata["r(1,1).var(1,1).GoodSegmentHi"].size1(), 1) = false;
			
		}

		//set in Excluded logical array
		for (int i = 0; i < shortseg.size1(); i++)
		{
			int start, end;
			start = GoodSegmentLoLinearIndex(shortseg(i, 0), 0);
			end = GoodSegmentHiLinearIndex(shortseg(i, 0), 0);
			int j=0;
			if (start > matrixdata["r(1,1).var(1,1).Excluded"].size1())
			{
				start -= matrixdata["r(1,1).var(1,1).Excluded"].size1();
				j = 1;
			}
			if (end > matrixdata["r(1,1).var(1,1).Excluded"].size1())
			{
				end -= matrixdata["r(1,1).var(1,1).Excluded"].size1();
				j = 1;
			}
			for (int iter = start; iter <= end; iter++)
				matrixdata["r(1,1).var(1,1).Excluded"](iter, j) = true;
		}
		// drop linear indexes
		matrix<double> tmpmatrix1(GoodSegmentLoLinearIndex.size1() - shortseg.size1(), 1, 0);
		matrix<double> tmpmatrix2(GoodSegmentHiLinearIndex.size1() - shortseg.size1(), 1, 0);
		for (int i = 0, j=0, pos=0; i < tmpmatrix1.size1() && j< GoodSegmentLoLinearIndex.size1(); i++, j++)
		{
			if (i == 110)
				i = i;
			if (pos < shortseg.size1())
				if (j == shortseg(pos)) { j++; pos++; }		
			tmpmatrix1(i, 0) = GoodSegmentLoLinearIndex(j, 0);
			tmpmatrix2(i, 0) = GoodSegmentHiLinearIndex(j, 0);
		}
		GoodSegmentLoLinearIndex = tmpmatrix1;
		GoodSegmentHiLinearIndex = tmpmatrix2;
		//Attach to est
		matrixdata["est.GoodSegmentLoLinearIndex"] = GoodSegmentLoLinearIndex;
		matrixdata["est.GoodSegmentHiLinearIndex"] = GoodSegmentHiLinearIndex;
		matrixdata["est.istartLinearIndex"] = matrix<double>(matrixdata["est.nstart"](0, 0), GoodSegmentLoLinearIndex.size1(), 0);	
		for (int i = 0; i < matrixdata["est.istartLinearIndex"].size1(); i++)
		{
			for (int j = 0; j < matrixdata["est.istartLinearIndex"].size2(); j++)
			{
				matrixdata["est.istartLinearIndex"](i, j) = GoodSegmentLoLinearIndex(j, 0) + i;			
			}
		}

		if (stringdata["fitoptions(1,1).ExcludedARMethod"].compare("MissingExtended") == 0)
		{
			//Used before 20070611
		}
		else if (stringdata["fitoptions(1,1).ExcludedARMethod"].compare("ExcludedExtended") == 0)
		{
			matrixdata["r(1,1).var(1,1).ExcludedAR"] = matrixdata["r(1,1).var(1,1).Excluded"];
			for (int i = 0; i < matrixdata["est.istartLinearIndex"].size1(); i++)
				for (int j = 0; j < matrixdata["est.istartLinearIndex"].size2(); j++)
				{
					int pos = matrixdata["est.istartLinearIndex"](i, j);
					if(pos < matrixdata["r(1,1).var(1,1).ExcludedAR"].size1())
						matrixdata["r(1,1).var(1,1).ExcludedAR"](pos, 0) = true;
					else
						matrixdata["r(1,1).var(1,1).ExcludedAR"](pos - matrixdata["r(1,1).var(1,1).ExcludedAR"].size1(), 1) = true;
				}						
		}
	}
	matrixdata["r(1,1).var(1,1).MissingNum"] = sum(matrixdata["r(1,1).var(1,1).Missing"]);
	matrixdata["r(1,1).var(1,1).ExcludedNum"] = sum(matrixdata["r(1,1).var(1,1).Excluded"]);
	matrixdata["r(1,1).var(1,1).ExcludedARNum"] = sum(matrixdata["r(1,1).var(1,1).ExcludedAR"]);
	matrixdata["r(1,1).var(1,1).GoodSegmentNum"] = sum(matrixdata["r(1,1).var(1,1).GoodSegmentLo"]);
}

void FitCreator::OneStep()
{
	if (stringdata["est.dInitialMethod"].compare("estprewhitening") == 0)
	{
		// Initial estimates for prewhitenfilter
		matrixdata["est.dInitialStep"] = matrix<double>(1, 1, true);
		estprewhitening();
	}
	matrixdata["est.ainit"].resize(2, matrixdata["est.ainit"].size2(), true);
	matrixdata["est.ainit"](1, 0) = matrixdata["est.ainit"](0, 0); matrixdata["est.ainit"](1, 1) = matrixdata["est.ainit"](0, 1);
	//matrixdata["bf.na"](0, 0) + matrixdata["est.nDfilt"](0, 0)
	int kbn;
	matrixdata["est.kbs"] = matrix<double>(matrixdata["est.nbs"](0, 0), 1);
	for (int i = 0; i < matrixdata["est.kbs"].size1(); i++)
	{
		matrixdata["est.kbs"](i, 0) = 1 + matrixdata["bf.na"](0, 0) + matrixdata["est.nDfilt"](0, 0) + i;
		kbn = matrixdata["est.kbs"](i, 0);
	}
	matrixdata["est.npfull"] =matrix<double>(1,1, matrixdata["bf.na"](0, 0) + matrixdata["est.nDfilt"](0, 0) + matrixdata["est.nbs"](0, 0) + matrixdata["est.nbn"](0, 0));
	matrixdata["est.kbn"] = matrix<double>(matrixdata["est.npfull"](0,0) - kbn, 1);
	for (int i = 0; i < matrixdata["est.kbn"].size1(); i++)
	{
		matrixdata["est.kbn"](i, 0) = 1+kbn+(double)i;
	}
	matrixdata["est.bfull"] = matrix<double>(matrixdata["est.npfull"](0, 0), 1, 0);
	matrixdata["est.bPriorMean"] = matrix<double>(matrixdata["est.npfull"](0, 0), 1, 0);
	matrixdata["est.bPriorW"] = matrix<double>(matrixdata["est.npfull"](0, 0), 1, 0);
	matrixdata["est.bisfree"] = matrix<double>(matrixdata["est.npfull"](0, 0), 1, true);
	estsetbisfree();
	if (matrixdata["bf.regseparate"](0, 0))
	{
		matrix<double> b(matrixdata["est.nbfTotal"](0,0)* matrixdata["s(1,1).multidim(1,1).p(1,1).nreg"]*2);
		for (int i = 0; i < b.size1(); i++)
		{
			b(i, 0) = 1 + (double)i;
		}
		matrixdata["est.kcoeff"] = bs2coeff(b);
	}
	else
	{
		matrixdata["est.kcoeff"] = bs2coeff(matrixdata["est.kbs"]);
	}
	for (int j = 0; j < 2; j++)
	{
		int ncap = matrixdata["est.ncap"](0, j);
		int df= matrixdata["est.df"](0, j);
		//Fitting channel
		matrix<double> GoodSegmentLo(matrixdata["r(1,1).var(1,1).GoodSegmentLo"].size1(), 1);
		for (int row = 0; row < GoodSegmentLo.size1(); row++)
			GoodSegmentLo(row) = matrixdata["r(1,1).var(1,1).GoodSegmentLo"](row, j);
		matrixdata["est.GoodSegmentLoIndex"] = trans(find(GoodSegmentLo));
		matrixdata["est.istart"] = matrix<double>(matrixdata["est.nstart"](0, 0), matrixdata["est.GoodSegmentLoIndex"].size2());

		for(int row=0; row< matrixdata["est.istart"].size1();row++)
			for (int column=0; column < matrixdata["est.istart"].size2(); column++)
			{
				matrixdata["est.istart"](row, column) = matrixdata["est.GoodSegmentLoIndex"](0, column) + row;
			}
		matrixdata["est.nstartseg"] = matrix<double>(1, 1, matrixdata["est.GoodSegmentLoIndex"].size2());
		matrixdata["est.Yoffset"] = matrixdata["r(1,1).var(1,1).Yoffset"];
		matrixdata["bf.Yscaler"] = matrixdata["r(1,1).var(1,1).Yscaler"];
		matrixdata["est.x.Jacobian"] = matrix<double>(matrixdata["r(1,1).var(1,1).dat"].size1(), matrixdata["est.npfull"](0, 0));
		string method = stringdata["est.InitMethod"];
		if (method.compare("ARXbil") == 0)
		{
			stringdata["est.op"] = "tplist";
			//fitinit							
		}
		matrix<double> bfull(matrixdata["est.npfull"](0,0), 1, NAN);
		bfull(0) = matrixdata["bf.a"](0, 0);
		bfull(1) = matrixdata["bf.a"](0, 1);
		for (int iter = 0; iter < matrixdata["est.nDfilt"](0, 0); iter++)
		{
			bfull(2+iter)= matrixdata["est.Dfilt"](1+iter,j);
		}
		matrixdata["est.bfull"] = bfull;
		matrixdata["est.bfree"] = matrixdata["est.bfull"];
		if (matrixdata["fitoptions(1,1).Kaufman"](0, 0))		//always 1 by optionsfile
		{
			// Linear parameters will be estimated anyway, just set to zero
			for (int iter = 0; iter < matrixdata["est.kbs"].size1(); iter++)
			{
				matrixdata["est.bfull"](matrixdata["est.kbs"](iter)-1) = 0;
			}
			for (int iter = 0; iter < matrixdata["est.kbn"].size1(); iter++)
			{
				matrixdata["est.bfull"](matrixdata["est.kbn"](iter)-1) = 0;
			}
		}
		else //
		{
			//Call modelfun for initial estimates of amplitudesand bn
		}
		matrixdata["est.bfree"] = matrixdata["est.bfull"];
		matrixdata["est.bfreeinit"] = matrixdata["est.bfree"];
		matrixdata["old.d"] = matrixdata["est.d"];
		matrixdata["old.bf"] = matrix<double>(1, 1, NAN);
		matrixdata["old.bn"] = matrix<double>(1, 1, NAN);
		matrixdata["old.fdat"] = matrix<double>(1,1,NAN);
		matrixdata["old.cost"] = matrix<double>(1, 1, NAN);
		matrixdata["est.lb"] = matrix<double>(matrixdata["est.np"](0, 0), 1, -INFINITY);
		matrixdata["est.ub"] = matrix<double>(matrixdata["est.np"](0, 0), 1, INFINITY);
		matrixdata["est.stop"] = matrix<double>(1, 1, false);
		matrixdata["est.gradnorm"]= matrix<double>(1, 1, NAN);
		matrixdata["est.iternum"]= matrix<double>(1, 1, 0);
		matrixdata["est.iterbasenum"]= matrix<double>(1, 1, 1);
		matrixdata["est.iter.p.perfsecTime"]= matrix<double>(1, 1, NAN);
		matrixdata["est.levlg"]= matrix<double>(1, 1, NAN);
		if (stringdata["fitoptions(1,1).OptimizationMethod"].compare("lsqnonlin") == 0) //always gnc by options file
		{
		}
		else if(stringdata["fitoptions(1,1).OptimizationMethod"].compare("fmincon") == 0)
		{
		}
		else if (stringdata["fitoptions(1,1).OptimizationMethod"].compare("fminunc") == 0)
		{
		}
		else if (stringdata["fitoptions(1,1).OptimizationMethod"].compare("GN") == 0)
		{
		}		
		else if (stringdata["fitoptions(1,1).OptimizationMethod"].compare("ML") == 0 || stringdata["fitoptions(1,1).OptimizationMethod"].compare("GNC") == 0)
		{
			matrixdata["est.stop"](0, 0) = 0;
			matrixdata["est.cost"] = matrix<double>(1, 1, NAN);
			matrixdata["est.stepfactor"] = matrix<double>(1, 1, NAN);
			matrixdata["est.dograd"] = matrix<double>(1, 1, 1);
			matrixdata["est.dohess"] = matrix<double>(1, 1, 1);
			if (stringdata["fitoptions(1,1).OptimizationMethod"].compare("ML") == 0)
			{
				matrixdata["est.lvlg"] = matrix<double>(1, 1, -9);
			}
			else
			{
				matrixdata["est.lvlg"] = matrix<double>(1, 1, -INFINITY);
			}	
			while (1)
			{			
				matrixdata["est.mainloop"] = matrix <double> (1,1,1);
				matrixdata["estBase.cost"] = matrixdata["est.cost"];
				modelfun(j);
				matrixdata["est.mainloop"] = matrix <double>(1, 1, 0);
				matrixdata["est.costRed"] = matrixdata["estBase.cost"]- matrixdata["est.cost"];
				if (matrixdata["est.cost"](0) > matrixdata["estBase.cost"](0))
				{
					// bad step, do not keep
					matrixdata["est.lvlg"](0, 0) = matrixdata["est.lvlg"](0, 0) + 1;
				}
				else
				{
					matrixdata["est.lvlg"](0, 0) = matrixdata["est.lvlg"](0, 0) - 1;
				}
				//matrix<double> F_bb_aug=


				matrixdata["est.bfree"] = matrixdata["est.bfree"] + matrixdata["est.db"];
			}
		}
	}
}

void FitCreator::estprewhitening()
{
	int XTX = 0;
	int XTY = 0;
	for (int j = 0; j < matrixdata["r(1,1).var(1,1).dat"].size2(); j++)
	{
		matrix<double> ix;
		matrix<double> bn;
		matrix<double> Y(matrix<double>(matrixdata["r(1,1).var(1,1).dat"].size1(), 1, 0));
		for (int i = 0; i < Y.size1(); i++)
		{
			Y(i, 0) = matrixdata["r(1,1).var(1,1).dat"](i, j);
		}
		matrix<double> Excluded(matrix<double>(matrixdata["r(1,1).var(1,1).Excluded"].size1(), 1, 0));
		for (int i = 0; i < Y.size1(); i++)
		{
			Excluded(i, 0) = matrixdata["r(1,1).var(1,1).Excluded"](i, j);
		}
		if (matrixdata["est.dInitialStep"](0, 0))
		{
			ix = find(Excluded);
			bn = mldivideminus(&matrixdata["est.x.Xn.var.dat"],&Y,&ix);
			Y -= prod(matrixdata["est.x.Xn.var.dat"], bn);
		}
		matrix<double> ExcludedAR(matrix<double>(matrixdata["r(1,1).var(1,1).ExcludedAR"].size1(), 1, 0));
		for (int i = 0; i < ExcludedAR.size1(); i++)
		{
			ExcludedAR(i, 0) = matrixdata["r(1,1).var(1,1).ExcludedAR"](i, j);
		}
		ix = find(ExcludedAR);
		matrixdata["est.nDfilt"]= matrixdata["est.nd"];
		matrix<double>nDfilt(1, 10);
		for (int i = 0; i < nDfilt.size2(); i++)
			nDfilt(0, i) = (double)i + 1;
		matrix<double> xj = delaycy(Y, nDfilt);
		matrix<double> XjTXj = trtimesminus(xj, *(new matrix<double>), ix);
		matrix<double> XjTYj = trtimesminus(xj, Y, ix);
		XjTXj = -XjTXj;
		//next three row do -XjTXj\XjTYj
		permutation_matrix<double> piv(XjTXj.size1());
		lu_factorize(XjTXj, piv);
		lu_substitute(XjTXj, piv, XjTYj);	
		for (int i = 1; i < matrixdata["est.Dfilt"].size1(); i++)
		{
			matrixdata["est.Dfilt"](i, j) = XjTYj(i - 1, 0);
		}
		
	}
	matrixdata["est.d"] = matrixdata["est.Dfilt"];
}

void FitCreator::estsetbisfree()
{
	matrixdata["est.np"] = sum(matrixdata["est.bisfree"]);
	matrixdata["est.df"] = matrix<double>(matrixdata["est.ncap"].size1(), matrixdata["est.ncap"].size2());
	for(int i=0; i< matrixdata["est.ncap"].size1(); i++)
		for (int j = 0; j < matrixdata["est.ncap"].size2(); j++)
		{
			matrixdata["est.df"](i,j)= matrixdata["est.ncap"](i,j) - matrixdata["est.np"](0,0);
		}
}

double summator(matrix<double>& a, matrix<double>& b, int i, int j)
{
	double val = 0;
	for (int counter = 0; counter < a.size2(); counter++)
		val += a(i, counter) * b(counter, j);
	return val;
}

 //model function for optimization, evaluates fit or cost, and derivatives
 //cost = mean - square whitened residual = z'*z/ncap.
 //Uses cost = z'*z/ncap instead of z' * z / (ncap - np) to be independent of np.
 //so cost = -2 * zVar / ncap * loglikelihood
 //Prior cost term is zVar / ncap * (a - aPriorMean)'*aPriorVar*(a-aPriorMean)
void FitCreator::modelfun(int colnum)
{
	for (int i = 0; i < matrixdata["est.bisfree"].size1(); i++)
		if (matrixdata["est.bisfree"](i) == 1)
			matrixdata["est.bfull"](i) = matrixdata["est.bfree"](i);


	matrix<double> bs(matrixdata["est.kbs"].size1(), 1);
	for (int i = 0; i < bs.size1(); i++) bs(i) = matrixdata["est.bfull"](matrixdata["est.kbs"](i) - 1);
	matrix<double> bn(matrixdata["est.kbn"].size1(), 1);
	for (int i = 0; i < bn.size1(); i++) bn(i) = matrixdata["est.bfull"](matrixdata["est.kbn"](i) - 1);
	matrixdata["est.pfilt"] = matrix<double>(1, 1, 1);

	//Evaluate basis functions
	basisfun5();
	matrixdata["est.td"] = matrix<double>(1, 1, 0);
	delaypolyreturnvalues* l = delaypoly(matrixdata["est.td"], matrixdata["est.dt"], "Delta");
	matrixdata["est.Ld"] = l->Ld;
	matrixdata["est.Ld_td"] = matrix<double>(1, 1, l->Ld_Td);
	matrixdata["est.Ld_tdtd"] = matrix<double>(1, 1, l->Ld_TdTd);
	delete l;
	matrixdata["est.Bpoly"] = matrixdata["bf.dat"];
	matrixdata["est.BLd"] = matrixdata["est.Bpoly"];
	matrix<double> estd(matrixdata["est.d"].size1(), 1);
	for (int i = 0; i < estd.size1(); i++) estd(i, 0) = matrixdata["est.d"](i, colnum);
	prewhitenstartmatrixreturnvalues dvalues;
	dvalues = prewhitenstartmatrix(estd, stringdata["fitoptions(1,1).DstartMethod"]);
	matrixdata["est.Dstart"] = dvalues.Ds;
	matrixdata["est.d"] = dvalues.DPoly;
	matrixdata["DstartExtra"] = dvalues.Dextra;
	std::vector<matrix<double>> DstartDeriv = dvalues.Dsderiv3D;
	matrix<double> dat(matrixdata["r(1,1).var(1,1).dat"].size1(), 1);
	for (int i = 0; i < dat.size1(); i++)
		dat(i, 0) = matrixdata["r(1,1).var(1,1).dat"](i, colnum);
	matrix<double> DY = prewhiten(dat, colnum, false); //yj.var.DY
	matrixdata["est.x.DXn.var.dat"] = prewhiten(matrixdata["est.x.Xn.var.dat"], colnum, false);
	matrix<double> ExcludedIndex = find(matrixdata["r(1,1).var(1,1).Excluded"]);
	matrixdata["est.x.ExcludedIndex"] = ExcludedIndex;
	calc_Xs();
	matrix<double> datcy = prewhiten(matrixdata["est.x.Xs.datcy"], colnum, true);
	matrixdata["est.x.DXs.datcy"] = datcy;
	matrix<double> DXs = cymatfull(matrixdata["est.x.DXs.datcy"]);
	matrixdata["est.x.DXs.dat"] = DXs;
	matrix<double> DXgood(matrixdata["est.x.DXn.var.dat"].size1(), matrixdata["est.x.DXn.var.dat"].size2() + DXs.size2());
	for (int row = 0; row < DXs.size1(); row++)
		for (int col = 0; col < DXs.size2(); col++)
		{
			if (DXs(row, col) > 1000)
				cout << row << " " << col << endl;
			DXgood(row, col) = DXs(row, col);
		}
	for (int row = 0; row < matrixdata["est.x.DXn.var.dat"].size1(); row++)
		for (int col = DXs.size2(); col < DXgood.size2(); col++)
		{
			if (matrixdata["est.x.DXn.var.dat"](row, col - DXs.size2()) > 1000)
				cout << row << " " << col << endl;
			DXgood(row, col) = matrixdata["est.x.DXn.var.dat"](row, col - DXs.size2());

		}
	for (int row = 0; row < ExcludedIndex.size1() / 2; row++)
		for (int col = 0; col < DXgood.size2(); col++)
		{
			DXgood(ExcludedIndex(row, colnum), col) = 0;
		}
	matrixdata["est.x.DXgood"] = DXgood;
	tempmatrix.resize(DXgood.size2(), DXgood.size1());
	for (int row = 0; row < DXgood.size1(); row++)
	{
		for (int col = 0; col < DXgood.size2(); col++)
		{
			tempmatrix(col, row) = DXgood(row, col);
		}
	}
	//default method (slow)
	//matrixdata["est.x.DXTDXgood"] = prod(tempmatrix, DXgood);
	//matrix<double> val(tempmatrix.size1(), DXgood.size2(), 0);

	matrix<double> val(tempmatrix.size1(), DXgood.size2(), 0);
	val = BigMatrixOperators::prod(tempmatrix, DXgood);
	matrixdata["est.x.DXTDXgood"] = val;

	//next three row do M/div
	matrix<double> blinear = prod(tempmatrix, DY);
	//next three row do  matrixdata["est.x.DXTDXgood"] / prod(tempmatrix, DY);
	permutation_matrix<double> piv(matrixdata["est.x.DXTDXgood"].size1());
	lu_factorize(matrixdata["est.x.DXTDXgood"], piv);
	lu_substitute(matrixdata["est.x.DXTDXgood"], piv, blinear);

	//matrix<double> blinear = matrixdata["est.x.DXTDXgood"] / prod(tempmatrix, DY);
	tempmatrix.clear();
	for (int row = 0; row < bs.size1(); row++)
		bs(row) = blinear(row);
	for (int row = 0; row < bn.size1(); row++)
		bn(row) = blinear(matrixdata["est.nbs"](0, 0) + row);
	if (matrixdata["bf.regseparate"](0, 0))
	{

	}
	else
	{
		for (int row = 0; row < bs.size1(); row++)
			matrixdata["est.bfull"](matrixdata["est.kbs"](row)) = bs(row);
		for (int row = 0; row < bn.size1(); row++)
			matrixdata["est.bfull"](matrixdata["est.kbn"](row) - 1) = bn(row);
	}
	matrixdata["est.x.bs_j"] = bs;

	// -Create elementary waveforms fj.var.dat = A * C
	if (matrixdata["bf.regseparate"](0, 0))
	{

	}
	else
	{
		if (stringdata["bf.code"].compare("kernel") == 0)
		{

		}
		else
		{
			//8933
			matrix<double> bfdat = matrixdata["bf.dat"];
			matrix<double> coeff(matrixdata["est.kcoeff"].size1(), matrixdata["est.kcoeff"].size2());
			for (int row = 0; row < coeff.size2(); row++)
			{
				for (int col = 0; col < coeff.size1(); col++)
				{
					coeff(col, row) = matrixdata["est.bfull"](matrixdata["est.kcoeff"](col, row), 0);
				}
			}
			matrix<double> a(1, 1, 1);
			boost::numeric::ublas::vector<double> pfilt(1, 1);
			bfdat = filter(pfilt, a, bfdat);
			matrix<double> bfdattotal = matrixdata["bf.dat"];
			tempmatrix.clear();
			tempmatrix.resize(1, coeff.size1() * coeff.size2(), 0);
			int c = 0;
			for (int row = 0; row < coeff.size1(); row++)
			{
				for (int col = 0; col < coeff.size2(); col++)
				{
					tempmatrix(0, c) = coeff(row, col);
					c++;
				}
			}
			matrix<double> dat = BigMatrixOperators::prod(bfdattotal, tempmatrix);
			matrixdata["fj.var.coeff"] = coeff;
			matrixdata["fj.var.dat"] = dat;
			matrixdata["est.x.bfdattotal"] = bfdattotal;

		}
	}
	matrixdata["est.iternum"](0, 0) = matrixdata["est.iternum"](0, 0) + 1;
	// -Create yj.var.eta, intermediate fitted signal
	matrix<double> test = cymatdatcyclic(matrixdata["est.x.Xs.datcy"]);
	matrix<double> eta = prod(test, trans(tempmatrix));
	matrixdata["yj.var.eta"] = eta;
	matrix<double> Jacobian = matrixdata["est.x.DXs.dat"];
	for (int col = 0; col < matrixdata["est.kbs"].size1(); col++)
	{
		for (int row = 0; row < Jacobian.size1(); row++)
			matrixdata["est.x.Jacobian"](row, col) = Jacobian(row, col);
	}
	matrixdata["yj.var.Ainv_eta"] = matrixdata["yj.var.eta"];
	//8981
	//-   Fitted signals
	postfun(matrixdata["yj.var.Ainv_eta"]);
	matrixdata["yj.var.ytrend"] = prod(matrixdata["est.x.Xn.var.dat"], bn);
	matrix<double> vardat(matrixdata["r(1,1).var(1,1).dat"].size1(), 1);
	for (int row = 0; row < vardat.size1(); row++)
		vardat(row) = matrixdata["r(1,1).var(1,1).dat"](row, colnum);
	matrixdata["yj.var.ydetrended"] = vardat - matrixdata["yj.var.ytrend"];
	matrixdata["yj.var.yfitted"] = matrixdata["yj.var.ysignal"] + matrixdata["yj.var.ytrend"];
	matrixdata["yj.var.efitted"] = vardat - matrixdata["yj.var.yfitted"];
	matrixdata["yj.var.Aefitted"] = matrixdata["yj.var.efitted"];
	matrix <double> z = prewhiten(matrixdata["yj.var.Aefitted"], 0, false);
	for (int i = 0; i < ExcludedIndex.size1() / 2; i++)
		z(ExcludedIndex(z.size1() * colnum + i)) = 0;
	matrixdata["yj.var.z"] = z;
	matrixdata["est.x.zj"] = z;
	matrix<double> zscaled = z / sqrt(matrixdata["est.ncap"](0, 0));
	//Residual sum of squares
	double ztz = 0;
	for (int i = 0; i < z.size1(); i++)
		ztz += pow(z(i), 2);
	matrixdata["est.zTz"] = matrix<double>(1, 1, ztz);
	matrixdata["est.zVar"] = matrixdata["est.zTz"] / matrixdata["est.df"](0, colnum);
	matrixdata["est.zSD"] = matrix<double>(1, 1, sqrt(matrixdata["est.zVar"](0, 0)));
	//Detfactor
	matrixdata["est.GoodSegmentNum"] = matrix<double>(1, 1, matrixdata["est.GoodSegmentLoIndex"].size2());
	matrixdata["est.AdetPower"] = matrix<double>(1, 1, -matrixdata["est.GoodSegmentNum"](0, 0) / matrixdata["est.ncap"](0, 0));
	double mult = 1;
	for (int i = 0; i < matrixdata["est.Dstart"].size1(); i++)
	{
		mult *= matrixdata["est.Dstart"](i, i);
	}
	matrixdata["est.Detlogmean"] = matrix<double>(1, 1, matrixdata["est.AdetPower"](0, 0) * 2 * log(mult));
	matrixdata["est.DetFactor"] = exp(matrixdata["est.Detlogmean"]);
	matrixdata["est.DetCost"] = prod(matrixdata["est.zVar"], matrixdata["est.Detlogmean"]);
	matrix<double> zclipped = z;
	matrixdata["fj.var.LincostNum"] = matrix<double>(1, 1, 0);
	matrixdata["est.FitCost"] = matrixdata["est.zTz"] / matrixdata["est.ncap"](0, 0);
	matrixdata["est.DetLterm"] = matrix<double>(1, 1, 0);
	matrixdata["est.DetJterm"] = matrix<double>(1, 1, 0);
	matrixdata["est.DetHterm"] = matrix<double>(1, 1, 0);
	matrix<double> cost = matrixdata["est.FitCost"];
	matrixdata["est.cost"] = cost;
	for (int i = 0; i < matrixdata["est.bfull"].size1(); i++)
	{
		if (matrixdata["est.bfull"](i) == 1)
			matrixdata["est.bree"](i) = matrixdata["est.bfull"](i);
	}
	matrix<double> oldcost = matrixdata["old.cost"];
	matrix<double> costRed = -(cost - oldcost) * pow(matrixdata["r(1,1).var(1,1).Yscaler"](0, 0), 2);
	double daMaxAbs = Max(abs(matrixdata["bf.a"] - matrixdata["est.old.bf.a"]));
	double dfMaxAbs;
	if (matrixdata["old.fdat"].size1() == matrixdata["fj.var.dat"].size1())
		dfMaxAbs = Max(abs(matrixdata["fj.var.dat"] - matrixdata["old.fdat"]));
	else
		dfMaxAbs = NAN;
	double dbMaxAbs;
	if (matrixdata["est.bfull"].size1() == matrixdata["old.bfull"].size1())
		dbMaxAbs = Max(abs(matrixdata["est.bfull"] - matrixdata["old.bfull"]));
	else
		dbMaxAbs = NAN;

	matrix<double> oldd = matrix<double>(matrixdata["old.d"].size1(), 1);
	for (int i = 0; i < oldd.size1(); i++)
		oldd(i) = matrixdata["old.d"](i, colnum);
	double ddMaxAbs = Max(abs(matrixdata["est.d"] - oldd));
	if (cost(0, 0) > oldcost(0, 0))
		return;

	double outstring[9];
	outstring[0] = matrixdata["est.iternum"](0, 0);
	outstring[1] = cost(0, 0);
	//if (!(abs(costRed) > matrixdata["est.dcostTol"](0, 0))(0, 0))
	outstring[2] = costRed(0, 0);
	//else
	//	outstring[2] = matrixdata["est.dcostTol"](0, 0); 

	//if (!(matrixdata["est.levlg"](0,0) > matrixdata["est.levlgTol"](0, 0)))
	outstring[3] = matrixdata["est.lvlg"](0, 0);
	//else
	//	outstring[3] = matrixdata["est.levlgTol"](0, 0);

	//if (!(dfMaxAbs > matrixdata["est.dfMaxAbsTol"](0, 0)))
	outstring[4] = dfMaxAbs;
	//	else
		//	outstring[4] = matrixdata["est.dfMaxAbsTol"](0, 0);

	//	if (!(matrixdata["est.gradnorm"](0,0) > matrixdata["est.gradnormTol"](0, 0)))
	outstring[5] = matrixdata["est.gradnorm"](0, 0);
	//	else
	//		outstring[5] = matrixdata["est.gradnormTol"](0, 0);

	//	if (!(daMaxAbs > matrixdata["est.daMaxAbsTol"](0, 0)))
	outstring[6] = daMaxAbs;
	//else
	//	outstring[6] = matrixdata["est.daMaxAbsTol"](0, 0);

	//if (!(dbMaxAbs > matrixdata["est.dbMaxAbsTol"](0, 0)))
	outstring[7] = dbMaxAbs;
	//	else
	//		outstring[7] = matrixdata["est.dbMaxAbsTol"](0, 0);

	//	if (!(ddMaxAbs > matrixdata["est.ddMaxAbsTol"](0, 0)))
	outstring[8] = ddMaxAbs;
	//	else
		//	outstring[8] = matrixdata["est.ddMaxAbsTol"](0, 0);


	if (matrixdata["est.iternum"](0, 0) == 1)
	{
		string Tolnames[9] = { "iter", "cost", "dcost", "levlg", "df", "grad", "da", "db", "dd" };
		printf("iter \t cost \t\t dcost \t\t levlg \t\t df \t grad \tda \t db \t dd \n");
	}
	printf("%g \t %g  \t %g  \t %g  \t %g  \t %g  \t %g  \t%g  \t %g \n", outstring[0], outstring[1], outstring[2], outstring[3], outstring[4], outstring[5], outstring[6], outstring[7], outstring[8]);
	//Store current values in old structure
	for (int i = 0; i < matrixdata["est.bisfree"].size1(); i++)
		if (matrixdata["est.bisfree"](i) == 1)
			matrixdata["est.bfree"](i) = matrixdata["est.bfull"](i);
	matrixdata["old.bfull"] = matrixdata["est.bfull"];
	matrixdata["old.bn"] =bn;
	matrixdata["old.d"] = matrixdata["est.d"];
	matrixdata["old.fdat"] = matrixdata["fj.var.dat"];
	matrixdata["old.cost"] = matrixdata["est.cost"];
	matrixdata["est.x.bs_j"] = bs;
	calc_Ja();
	matrixdata["est.x.ej"] = matrixdata["yj.var.Aefitted"];
	calc_Jd();
}

matrix<double> FitCreator::cholderiv(const matrix<double> A)
{
	matrix<double> R = chol(A);
	return R;
}

void FitCreator::cholderiv(const matrix<double> A, std::vector<matrix<double>> AD, matrix<double> *R, std::vector<matrix<double>> *Rd)
{
	*R = chol(A);
	int p = A.size1();
	int nq = AD.size() * AD[0].size1() * AD[0].size2() / pow(p, 2);	
	matrix<double> ii(p, p);	
	int val = 0;
	for (int i = 0; i < p; i++)
		for (int j = 0; j < p; j++)
			ii(i, j) = val++;
	matrix<double> iupper(p, p);
	for (int i = 0; i < p; i++)
		for (int j = i; j < p; j++)
			iupper(i, j) = 1;
	matrix<double> RTkronI=kron(trans(*R),speye(p));
	matrix<double> IkronRT=kron(speye(p), trans(*R));
	int size = 0;
	for (int i = 1; i <= p; i++)
		size += i;
	std::vector<double> vRow;
	std::vector<double> vCol;
	int pos=0;
	for (int j = 0; j < p; j++)
		for (int i = 0; i < p; i++)
		{
			if (iupper(i, j) == 1)
			{
				vRow.push_back(pos);
				vCol.push_back(ii(i, j));
			}
			pos++;
		}
	matrix<double> M(size, size);
	matrix<double> RTkronItemp(size, size);
	
	for (int j = 0; j < size; j++)
		for (int i = 0; i < size; i++)	
			RTkronItemp(i, j) = RTkronI(vRow[i], vCol[j]);
	matrix<double> IkronRTtemp(size, size);
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			IkronRTtemp(i, j) = IkronRT(vRow[i], vRow[j]);
	M = RTkronItemp + IkronRTtemp;
	matrix<double> RD(pow(p, 2), nq, 0);
	matrix<double> tempAdtriu(pow(p, 2), nq);
	for (int z = 0; z < AD.size(); z++)
		for (int j = 0; j < AD[z].size2(); j++)
			for (int i = 0; i < AD[z].size1(); i++)
				tempAdtriu(j*AD[z].size1()+i, z) = AD[z](i, j);
	matrix<double> Adtriu(size, nq);
	for (int i = 0; i < Adtriu.size1(); i++)
		for (int j = 0; j < Adtriu.size2(); j++)
			Adtriu(i, j) = tempAdtriu(vRow[i], j);
	matrix<double> div = Adtriu;
	//next three row do M/div
	permutation_matrix<double> piv(M.size1());
	lu_factorize(M, piv);
	lu_substitute(M, piv, div);
	for (int i = 0; i <div.size1(); i++)
		for (int j = 0; j < div.size2(); j++)
			RD(vRow[i], j) = div(i, j);
	for (int z = 0; z < AD.size(); z++)
	{
		matrix<double> inval(nq, nq);
		for (int j = 0; j < nq; j++)
			for (int i = 0; i < nq; i++)
				inval(i, j) = RD(j * nq + i, z);
		Rd->push_back(inval);
	}
}

matrix<double> FitCreator::prewhiten(matrix<double> X, int colnum, bool dat)
{
	matrix<double> DXdat(X.size1(), X.size2());
	matrixdata["est.prewhitenscaler"] = matrix<double>(1, 1, 1);
	matrixdata["est.nd"] = matrix<double>(1, 1, matrixdata["est.d"].size1() - 1);
	matrix<double> dlag(matrixdata["est.d"].size1(), 1);
	for (int i = 0; i < dlag.size1(); i++)
		dlag(i) = i;
	matrixdata["est.dlag"] = dlag;
	if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("DstartGoodseg") == 0)
	{

	}
	else if(stringdata["fitoptions(1,1).prewhitenMethod"].compare("DstartExclude") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Simple") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Offset") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Segment") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("GoodSegment") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Segment3") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Segment4") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Segment2") == 0 || stringdata["fitoptions(1,1).prewhitenMethod"].compare("SegmentChol") == 0)
	{

	}
	else if (stringdata["fitoptions(1,1).prewhitenMethod"].compare("Segmentfft") == 0 )
	{
		matrix<double> Dstart(matrixdata["est.nd"](0, 0), matrixdata["est.nd"](0, 0), 0);
		prewhitenstartmatrixreturnvalues dvalues;
		matrix<double> estd(matrixdata["est.d"].size1(), 1);
		for (int i = 0; i < estd.size1(); i++) estd(i, 0) = matrixdata["est.d"](i, colnum);
		dvalues = prewhitenstartmatrix(estd, stringdata["fitoptions(1,1).DstartMethod"]);
		Dstart = dvalues.Ds;
		estd = dvalues.DPoly;

		if (stringdata["fitoptions(1,1).DstartMethod"].compare("zero") == 0 || stringdata["fitoptions(1,1).DstartMethod"].compare("DstartExclude") == 0)
			matrixdata["est.ExcludedStartNum"] = matrix<double>(1, 1, matrixdata["est.istart"].size1() * matrixdata["est.istart"].size2());
		else
			matrixdata["est.ExcludedStartNum"] = matrix<double>(1, 1, 0);
		//	fill(&DXdat, 0);	
		if (!dat)
		{
			int ncols = DXdat.size2();
			matrix<double>Xj(DXdat.size1(), 1);
			for (int j = 0; j < ncols; j++)
			{
				for (int i = 0; i < DXdat.size1(); i++)  Xj(i) = X(i, j);
				//filter over all segments
				matrix<double> a(matrixdata["est.d"].size1(), 1, 0);
				a(0) = 1;
				boost::numeric::ublas::vector<double> estdv(matrixdata["est.d"].size1());
				for (int i = 0; i < estdv.size(); i++)
					estdv(i) = matrixdata["est.d"](i, 0);
				matrix<double> DXj = filter(estdv, a, Xj);
				// replace start of each segment with Ds* xstart
				//  DXj(est.istart)=est.Dstart(:, :, jd)*Xj(est.istart);
				matrix<double> xjistart(matrixdata["est.istart"].size1(), matrixdata["est.istart"].size2());
				for (int i = 0; i < matrixdata["est.istart"].size1(); i++)
					for (int k = 0; k < matrixdata["est.istart"].size2(); k++)
					{
						xjistart(i, k) = Xj(matrixdata["est.istart"](i, k));
					}
				tempmatrix = prod(matrixdata["est.Dstart"], xjistart);
				for (int i = 0; i < tempmatrix.size1(); i++)
					for (int k = 0; k < tempmatrix.size2(); k++)
						DXj(matrixdata["est.istart"](i, k)) = tempmatrix(i, k);
				for (int i = 0; i < DXdat.size1(); i++)
					DXdat(i, j) = DXj(i);
			}
		}
		else
		{
			matrix<double> DX = cymatfilt(matrixdata["est.d"],X,"filter");	

			matrix<double> iextra = matrixdata["est.istart"];
			int nextra = iextra.size1() * iextra.size2();
			matrix<double> Xstart = cymatdatcyclic(X, iextra);	
			int r = matrixdata["est.nd"](0, 0);
			int c = Xstart.size1() * Xstart.size2() / matrixdata["est.nd"](0, 0);
			matrix<double> Xstarttemp(r,c);	

			Xstarttemp = reshape(Xstart, r, c);
			matrix<double> DXstarttemp = prod(matrixdata["est.Dstart"], Xstarttemp);		
			matrix<double> DXstart = reshape(DXstarttemp, Xstart.size1(), Xstart.size2());
			//tempmatrix = cymatdatcyclic(X, iextra);
			//datextra.resize(DXstart.size1(), DXstart.size2());
			//for(int row=0; row< datextra.size1(); row++)
			//	for (int col = 0; col < datextra.size2(); col++)
			//datextra(row,col) = DXstart(row, col) - tempmatrix(row, col);
			datextra = DXstart - cymatdatcyclic(X, iextra);
			DXdat = DX;
		}
		
	}
	return DXdat;
}


void FitCreator::calc_Xs()
{
	if (matrixdata["bf.regseparate"](0, 0))
	{
		//%% - Xs by ifftx
		//something 
	}
	else
	{
		if (stringdata["bf.code"].compare("kernel") == 0)
		{			
			// bs indexing is [xdat lag reglag]
			//copy some structs
			//est.x.Xs = est.x.xcymat;
			//est.x.Xs.lagcy = add((bf.kmin:bf.kmax)', est.reglag(:)');
		}
		else
		{
			matrixdata["est.x.Xs.datcy"] = matrixdata["est.x.xcymat.datcy"];
			matrixdata["est.x.Xs.lagcy"] = matrixdata["est.x.xcymat.lagcy"];
			matrixdata["est.x.Xs.CycleNum"] = matrixdata["est.x.xcymat.CycleNum"];
			matrixdata["est.x.Xs.CycleLoIndex"] = matrixdata["est.x.xcymat.CycleLoIndex"];
			matrixdata["est.x.Xs.CycleHiIndex"] = matrixdata["est.x.xcymat.CycleHiIndex"];

			for (int kbf = 0; kbf < matrixdata["est.nbf"](0, 0); kbf++)
			{
				matrix<double> Bpoly(matrixdata["est.Bpoly"].size1(), 1);
				for (int row = 0; row < matrixdata["est.Bpoly"].size1(); row++)
					Bpoly(row) = matrixdata["est.Bpoly"](row, kbf);
				matrix<double>XFilt = cymatfilt(Bpoly, matrixdata["est.x.Xs.datcy"], "fft");
				matrixdata["est.x.Xs.datcy"] = XFilt;	
			}
		}
	}
}

matrix<double> FitCreator::cymatfilt(matrix<double> b, matrix<double> datcy, string method)
{
	matrix<double> dat = datcy;
	if (method.compare("ffth") == 0)
	{

	}
	else if (method.compare("fft") == 0)
	{
		int nfft = dat.size1();
		matrix<std::complex<double>> Fx(nfft, dat.size2());
		matrix<std::complex<double>> Fb(nfft, b.size2());
		matrix<std::complex<double>> Fy(Fx.size1(),Fx.size2());
		Fx = fft(dat, nfft);	
		Fb = fft(b, nfft);
		for (int j = 0; j < Fx.size2(); j++)	
			for (int i = 0; i < nfft; i++)		
				Fy(i, j) = Fx(i, j) * Fb(i);
				
		Fy = ifft(Fy, Fy.size1());
		for (int j = 0; j < Fx.size2(); j++)
		{
			for (int i = 0; i < nfft; i++)
			{
				dat(i,j) = Fy(i,j).real();
			}
		}
	}
	else if (method.compare("filter") == 0)
	{
		for (int j = 0; j < dat.size2(); j++)
		{
			boost::numeric::ublas::vector<double>bj(b.size1());
			copy(b.begin1(), b.end1(), bj.begin());
			int ilo = 1;
			int ihi = dat.size1();
			matrix<double>xWithPrelude(dat.size1() + b.size1(), 1);
			int pos=0;
			for (int i = ihi - b.size1(); i < dat.size1(); i++, pos++)
				xWithPrelude(pos) = dat(i, j);
			for (int i = 0; i < dat.size1(); i++, pos++)
				xWithPrelude(pos) = dat(i, j);
			matrix<double> a(bj.size(), 1, 0);
			a(0) = 1;
			matrix<double>yWithPrelude = filter(bj, a, xWithPrelude);
			for (int i = 0; i < dat.size1(); i++)
			{
				dat(i, j) = yWithPrelude(b.size1() + i);
			}
		}
	}
	return dat;
}

matrix<double> FitCreator::cymatfull(matrix<double> x)
{
	matrix<double> dat = cymatdatcyclic(x);
	return dat;
}

matrix<double> FitCreator::cymatdatcyclic(matrix<double> x)
{
	//matrix<double> dat = delaycy(x, matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"]);
	matrix<double> dat = delaycy(x, matrixdata["est.x.Xs.lagcy"]);
	return dat;
}

matrix<double> FitCreator::cymatdatcyclic(matrix<double> x, matrix<double> i)
{
	matrix<double> dat = delaycy(x, matrixdata["est.x.Xs.lagcy"], i);
	return dat;
}

void FitCreator::postfun(matrix<double> eta)
{
	matrixdata["yj.var.ysignal"] = eta;
	matrixdata["yj.var.fs_eta"] = matrix<double>(1,1, 1);
	matrixdata["yj.var.fs_postfunpar"] = matrix<double>(0,0);
}

void FitCreator::calc_Ja()
{
	matrix<double> bs_j=reshape(matrixdata["est.x.bs_j"], 2, matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"].size2());
	matrix<double> bs_j_nx_nbf = matrixdata["est.x.bs_j"];
	matrix<double> X = cymatdatcyclic(matrixdata["est.x.xcymat.datcy"]);
	matrix<double> vdat = prod(X, bs_j_nx_nbf);
	matrix<double> fs_adat(X.size1(),2,0);
	matrix<double> vkbfdat = vdat;
	matrix<double>  vkbf_filt;
	for (int k = 0; k< matrixdata["bf.afree"].size1(); k++)
	{
		matrix<double> bf_J = matrix<double>(matrixdata["bf.J"].size1(), 1);
		for (int i = 0; i< matrixdata["bf.J"].size1(); i++)	
			bf_J(i) = matrixdata["bf.J"](i,k);
		vkbf_filt = cymatfilt(bf_J, vdat, "fft");
		for (int i = 0; i < fs_adat.size1(); i++) 
			fs_adat(i, k) += vkbf_filt(i);
	}

	matrix<double> Ja = prewhiten(fs_adat, 0, false);
	for (int i = 0; i < matrixdata["est.x.ExcludedIndex"].size1() / 2; i++)
	{
		Ja(matrixdata["est.x.ExcludedIndex"](i), 0) = 0;
		Ja(matrixdata["est.x.ExcludedIndex"](i), 1) = 0;
	}

	matrix<double> Jacobian = cymatdatcyclic(Ja);
	for (int i = 0; i < Jacobian.size1(); i++)
		for (int j = 0; j < Jacobian.size2(); j++)
			matrixdata["est.x.Jacobian"](i, j) = Jacobian(i, j);

}

void FitCreator::calc_Jd()
{
	prewhitenstartmatrixreturnvalues values = prewhitenstartmatrix(matrixdata["est.d"], "Chol");
	matrix<double> X = -matrixdata["est.x.ej"];
	int nDfiltfree = matrixdata["est.nDfiltfree"](0,0);
	matrix<double>DX_d(X.size1(), nDfiltfree, 0);
	matrix<double>Xstart(nDfiltfree, matrixdata["est.istart"].size2(), 0);
	for (int row = 0; row < nDfiltfree; row++)
	{
		for (int col = 0; col < matrixdata["est.istart"].size2(); col++)
		{
			Xstart(row, col) = X(matrixdata["est.istart"](row, col));
		}
	}
	
	for (int k = 0; k < nDfiltfree; k++)
	{
		matrix<double> d = delaycy(X, matrix<double>(1, 1, k+1));
		cout << d << endl;
		for (int row = 0; row < DX_d.size1(); row++)
			DX_d(row, k) = d(k);
		matrix<double> dsDeriv(values.Dsderiv3D.size(), values.Dsderiv3D.size());
		for (int row = 0; row < values.Dsderiv3D.size(); row++)
			for (int col = 0; col < values.Dsderiv3D.size(); col++)
				dsDeriv(row, col) = values.Dsderiv3D[k](row, col);
		matrix<double> dsxst = prod(dsDeriv, Xstart);
		for (int row = 0; row < nDfiltfree; row++)
		{
			for (int col = 0; col < matrixdata["est.istart"].size2(); col++)
			{
				DX_d(matrixdata["est.istart"](row, col),k) = dsxst(row,col);
			}
		}
		matrix<double> JdDat = DX_d;
	}
	//10544
}


FitCreator::prewhitenstartmatrixreturnvalues FitCreator::prewhitenstartmatrix(matrix<double> Dpoly, string method)
{
	prewhitenstartmatrixreturnvalues *w= new prewhitenstartmatrixreturnvalues;
	//order of AR filter
	w->DPoly = Dpoly;
	int p = Dpoly.size1() - 1;
	int nout = p;
	//check that zeros are inside unit circle, if necessary
	matrix<std::complex<double>> zroots = roots(Dpoly);
	matrix<double> zrootsabs=abs(zroots);
	matrix<double> kfix = zrootsabs >= 1;
	if (sum(kfix)(0, 0) > 0)
	{
		//some error 
		//parameters that were not initialized are used in matlab 
		//18184
	}
	int nt = 2 * p;
	matrix<double> dpadzero(nt, 1, 0);
	for (int i = 0; i < Dpoly.size1(); i++)
	{
		dpadzero(i, 0) = Dpoly(i, 0);
	}
	//index vectors
	matrix<double> k1(p, 1);
	matrix<double> k2(p, 1);
	matrix<double> kend(p, 1);
	matrix<double> kflip(p, 1);
	for (int i = 0; i < p; i++)
	{
		k1(i, 0) = i;
		k2(i, 0) = p + i;
		kend(i, 0) = nt - p + i;
		kflip(i, 0) = p -1- i;
	}
	matrix<double> dclip(p, 1);
	for (int i = 0; i < p; i++)
	{
		dclip(i, 0) = Dpoly(k1(i, 0));
	}

	matrix<double>k(p, 1);
	for (int i = 0; i < p; i++)
		k(i) = i;
	matrix<double> dpadzerodelay = delay(dpadzero,k);
	matrix<double> D(k1.size1(), k1.size1());
	matrix<double> B(k2.size1(), k1.size1());
	for (int i = 0; i < k1.size1(); i++)
	{
		for (int j = 0; j < k2.size1(); j++)
		{
			D(i, j) = dpadzerodelay(k1(i), k1(j));
			B(i, j) = dpadzerodelay(k2(i), k1(j));
		}
	}
	matrix<double> BM(B.size1(), B.size2());
	//early return methods
	if (method.compare("zero") == 0 || method.compare("DstartZero") == 0 || method.compare("DstartExclude") == 0)
	{
		w->Ds.resize(nout, nout);
		fill(&w->Ds, 0);
		w->Dextra = -D;
		w->Dsderiv.resize(3, 1);
		w->Dsderiv(0) = nout; w->Dsderiv(1) = nout; w->Dsderiv(2) = p;	
		return *w;
	}
	else if (method.compare("none") == 0)
	{
		w->Ds = D;
		w->Dextra.resize(nout, nout);
		fill(&w->Dextra, 0);
		//18243 check dsderiv if need
		return *w;
	}

	if (method.compare("all") == 0)
	{

	}
	else if (method.compare("cholW") == 0 || method.compare("chol") == 0 || method.compare("Chol") == 0 || method.compare("Chol4") == 0)
	{
		for(int i=0; i<BM.size1(); i++)
			for (int j = 0; j < BM.size2(); j++)
				BM(i, j) = B(i, kflip(j));
		matrix<double>  MBTBM = prod(trans(BM), BM);
		matrix<double>  DTD = prod(trans(D) , D);
		matrix<double> DTD_MBTBM = DTD - MBTBM;
		matrix<double> MDsM = cholderiv(DTD_MBTBM);
		w->Ds.resize(MDsM.size1(), MDsM.size2());
		for (int i = 0; i < MDsM.size1(); i++)
			for (int j = 0; j < MDsM.size1(); j++)
				w->Ds(i, j) = MDsM(kflip(i), kflip(j));

	}
	else if (method.compare("Chol2") == 0)
	{
	}
	else if (method.compare("Chol3") == 0)
	{
	
	}
	else if (method.compare("Chol1") == 0)
	{

	}
	else if (method.compare("Segment2") == 0|| method.compare("qr") == 0)
	{

	}
	else if (method.compare("SegmentChol") == 0 || method.compare("cholVinv") == 0)
	{

	}
	else if (method.compare("eig") == 0)
	{

	}
	matrix<double> dnout(nout, 1);
	for (int i = 0; i < dnout.size1(); i++)
		dnout(i, 0) = Dpoly(i, 0);
	w->Dextra = w->Ds - delay(dnout, k);
	matrix<double> i(k1.size1(), k1.size1()), j(k1.size1(), k1.size1());
	for(int row=0; row< k1.size1(); row++)
		for (int col = 0; col < k1.size1(); col++)
		{
			j(row, col) = k1(col);
			i(col, row) = k1(col);
		}
	matrix<double> iminusj = i - j;
	// let dshift be such that dshift(dshiftbase + k) = dk, k = 0:p, also k<0, k>p
	int dshiftbase = p + 7;
	matrix<double> dshift((dshiftbase - 1) + Dpoly.size1() + (p - 1), 1, 0);
	for (int row = 0; row < Dpoly.size1(); row++)
		dshift(dshiftbase - 1 + row,0) = Dpoly(row);
	for (int row = 0; row < k1.size1(); row++)
		for (int col = 0; col < k1.size1(); col++)
		{
			D(row, col) = dshift(dshiftbase + iminusj(row, col)-1);	
		}
	matrix<double> DPadded(D.size1() + p, p,0);
	for (int row = 0; row < D.size1(); row++)
		for (int col = 0; col < D.size2(); col++)
			DPadded(row, col) = D(row, col);
	tempmatrix = trans(B);
	matrix<double> BTpadded(B.size1() + p, p, 0);
	for (int row = 0; row < tempmatrix.size1(); row++)
		for (int col = 0; col < tempmatrix.size2(); col++)
			BTpadded(row, col) = tempmatrix(row, col);
	matrix<double>RTR = prod(trans(D), D) - prod(trans(BM), BM);
	std::vector<matrix<double>> Dsderiv;
	matrix<double>x(nout, 1);
	for (int row = 0; row < tempmatrix.size1(); row++)
		x(row) = row;
	tempmatrix = delay(x, x);	
	matrix<double> input(nout, nout);
	for (int iter = 0; iter < nout; iter++)
	{
		fill(&input, iter + (int)1);		
		input = tempmatrix - input;
		input = input == 0;
		Dsderiv.push_back(input);
	}
	std::vector<matrix<double>> RTR_d;
	matrix<double> zeromatrix(p, p, 0);
	for (int iter = 0; iter < p; iter++)
	{
		RTR_d.push_back(zeromatrix);
	}
	for (int k = 0; k < p; k++)
	{
		matrix<double> D_dkT_D(k1.size1(), DPadded.size2());
		for (int row = 0; row < D_dkT_D.size1(); row++)
			for (int col = 0; col < D_dkT_D.size2(); col++)
				D_dkT_D(row, col) = DPadded(k1(row) + k+1, col);
		matrix<double> BM_dkT_BM(k1.size1(), BTpadded.size2());
		for (int row = 0; row < BM_dkT_BM.size1(); row++)
			for (int col = 0; col < BM_dkT_BM.size2(); col++)
				BM_dkT_BM(row, col) = BTpadded(k1(row) +p-k-1, col);
		matrix<double> RTR_dk = (D_dkT_D + trans(D_dkT_D)) - (BM_dkT_BM + trans(BM_dkT_BM));
		RTR_d[k] = RTR_dk;
		}
	matrix<double> R;
	std::vector<matrix<double>> R_d;
	cholderiv(RTR, RTR_d, &R, &R_d);
	matrix<double> flipmatrix(p,p);
	for (int z = 0; z < R_d.size(); z++)
	{
		for (int row = 0; row < R_d[z].size1(); row++)
			for (int col = 0; col < R_d[z].size2(); col++)		
				flipmatrix(row,col) = R_d[z](kflip(row), kflip(col));
		Dsderiv[z] = flipmatrix;									
	}
	w->Dsderiv3D = Dsderiv;
	return *w;
}

FitCreator::delaypolyreturnvalues* FitCreator::delaypoly(matrix<double> Td, matrix<double> dt, string LdMode)
{
	double Ld_Td, Ld_TdTd;
	matrix<double> Ld;
	if (LdMode.compare("Delta") == 0)
	{
		matrix<double> d = dt;
		d(0, 0) = Td(0,0) / dt(0,0);
		//if be error here add check for size matrix
		/*permutation_matrix<double> piv(Td.size1());
		lu_factorize(Td, piv);
		lu_substitute(Td, piv, d);*/
		int dsize[2] = { d.size1(), d.size2()};
		matrix<double> p = ceil(d);
		double n = Max(p) + 1;
		matrix<double> k(1, n);
		for (int i = 0; i < k.size2(); i++)
			k(0, i) = i;
		matrix<double> r = k - d;
		matrix<double> Ldsize(1, 1 + d.size2(), 1);
		matrix<bool> il = (-0.5 < r) & (r <= 0.5);
		Ld.resize(Ldsize(0, 0), Ldsize(0, 1));
		for (int i = 0; i < Ld.size1(); i++) for (int j = 0; j < Ld.size2(); j++) Ld(i, j) = 0;
		for (int i = 0; i < il.size1(); i++)
			Ld(0, il(i) - 1) = 1;
		Ld_Td = 0;
		Ld_TdTd = 0;
	}
	else if (LdMode.compare("TPS4") == 0)
	{

	}
	else if (LdMode.compare("P2") == 0)
	{

	}
	else if (LdMode.compare("P3") == 0)
	{

	}
	delaypolyreturnvalues *val = new delaypolyreturnvalues{ Ld, Ld_Td, Ld_TdTd };
	return val;
}

matrix<double> FitCreator::bs2coeff(matrix<double> bs)
{
	matrix<double> coeff(bs.size1() / 2, 2);
	for (int i = 0, j = 0; i < coeff.size1(); i++, j=j+2)
	{
		coeff(i, 0) = bs(j, 0);
		coeff(i, 1) = bs(j+1, 0);
	}
	return coeff;
}

matrix<double> FitCreator::mldivideminus(matrix<double> *x, matrix<double> *y, matrix<double> *ibad)
{
	matrix<double>xtx = prod(trans(*x), (*x));
	matrix<double>xty = prod(trans(*x), (*y));
	if (ibad->size1() !=0)
	{

		matrix<double> xbad(ibad->size1(), x->size2());
		matrix<double> ybad(ibad->size1(), y->size2());
		for (int i = 0; i < xbad.size1(); i++)
			for (int j = 0; j < xbad.size2(); j++)			
				xbad(i, j) = (*x)((*ibad)(i),j);
		for (int i = 0; i < ybad.size1(); i++)
			for (int j = 0; j < ybad.size2(); j++)
				ybad(i, j) = (*y)((*ibad)(i), j);
		xtx-= prod(trans(xbad), xbad);
		xty-= prod(trans(xbad), ybad);
	} 
	//next three row do xtx/xty
	permutation_matrix<double> piv(xtx.size1());
	lu_factorize(xtx, piv);
	lu_substitute(xtx, piv, xty);
	return xty;
}

matrix<double> FitCreator::readfile(string file)
{
	boost::filesystem::ifstream infile(file);
	std::string line;
	std::vector<string> vect;
	int SegmentNumber = 0; 
	std::streampos oldpos;
	while (1)
	{
		oldpos = infile.tellg();
		std::getline(infile, line);
		if (line[0] != '%')
			break;
		else
		{
			removeSpaces(line);
			split(line, vect, ' ');

			if (vect.size() != 0)
			{
				vect[0].erase(0, 1);
				if (vect[0]=="Segment")
				{
					SegmentNumber++;
					int iseg = atoi(vect[1].c_str()) + 1;
					stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).StartDatenum{" +to_string(iseg)+ ",1}"] = vect[3];
					stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).EndDatenum{" + to_string(iseg) + ",1}"] = vect[5];
					if (vect.size() >= 10 && vect[6] == "Decay")
					{					
						stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).DecayTop{" + to_string(iseg) + ",1}"] = vect[7];
						stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).DecayTop{" + to_string(iseg) + ",2}"] = vect[8];
						stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).DecayBottom{" + to_string(iseg) + ",1}"] = vect[9];
						stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).DecayBottom{" + to_string(iseg) + ",2}"] = vect[10];
					}				
				}
				else
					if (vect[0]=="SchemaVersion" || vect[0]=="Protocol"|| vect[0]=="MRN")
					{
						stringdata["r(1,1).p(1,1).RecordingInfo(1,1)."+vect[0]] = vect[1];
					}
					else
						if (vect[0]=="DateOfBirth")
						{
							stringdata["r(1,1).p(1,1).RecordingInfo(1,1).DOB"] = vect[1];
						}
						else
							if (vect[0]=="DateOfTest")
							{
								stringdata["r(1,1).p(1,1).RecordingInfo(1,1).VisitDate"] = vect[1];
							}
							else
								if (vect[0]=="Gender")
								{
									if (vect[1]=="M" || vect[1]=="Male")
									{
										matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).sex"] = matrix<double>(1, 1, 1);
									}
									else
										if (vect[1]=="F" || vect[1]=="Female")
										{
											matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).sex"] = matrix<double>(1, 1, 2);
										}
										else
											matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).sex"] = matrix<double>(1, 1, 0);
									
								}
								else
									if (vect[0]=="Age" || vect[0]=="IPD" || vect[0]=="Duration" || vect[0]=="RxCylindrical" || vect[0]=="RxSpherical" || vect[0]=="RxAngle" || vect[0]=="SphericalEq" || vect[0]=="SuggestedTrialLens" || vect[0]=="ActualTrialLens")
									{
										for (int i = 0; i < vect.size() - 1; i++)
										{
											matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).VisitDate(1,1)." + vect[0]+"{1,"+ to_string(i+1) + "}"] = matrix<double>(1, 1, stod(vect[i+1]));
										}
									}
									else
										if (vect[0]=="BCVA")
										{
											for (int i = 0; i < vect.size() - 1; i++)
											{
												std::vector<string> ans;
												if (split(vect[i + 1], ans, '/') >= 1)
												{
													double a = stod(ans[0]);
													double b = stod(ans[1]);
													double val = 20 * b / a;
													matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).BCVAfeet{1," + std::to_string(i + 1) + "}"] = matrix<double>(1, 1, val);
												}
												else
													matrixdata["r(1,1).p(1,1).RecordingInfo(1,1).BCVAfeet{1," + std::to_string(i + 1) + "}"] = matrix<double>(1, 1, 'NaN');
											}
										}
										else
											if (vect[0]=="ValidData")
											{	
												for (int i = 0; i < vect.size() - 1; i++)
												{
													stringdata["r(1,1).p(1,1).RecordingInfo(1,1).ValidPerc{1," + std::to_string(i + 1) + "}"] = vect[i + 1];
												}
											}
											else
											{
												if (vect[0]=="Correction")
												{

												}
												else
													if (vect[0]=="Prescription")
													{

													}
													else
													{
														for (int i = 0; i < vect.size() - 1; i++)
														{
															stringdata["r(1,1).p(1,1).RecordingInfo(1,1)." + vect[0] + "{1," + std::to_string(i + 1) + "}"] = vect[i + 1];
														}
													}
																						
											}
			}
		}
		vect.clear();
	}
	
	matrixdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).PrecedingGapSec"] = matrix<double>(1, SegmentNumber, 0);

	for (int i = 0; i < SegmentNumber-1; i++)
	{
		ptime start = from_iso_string(stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).StartDatenum{" + to_string(i + 2) + ",1}"]);	
		ptime end = from_iso_string(stringdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).EndDatenum{" + to_string(i + 1) + ",1}"]);
		time_duration dur = start - end;
		matrixdata["r(1,1).p(1,1).SegTbl(1,1).var(1,1).PrecedingGapSec"](0, i) = dur.total_seconds();
	}	
	
	//std::vector<double> column;
	//std::vector<std::vector<double>> row;
	string tempstr;
	removeSpaces(line);
	int cols = split(line, vect, ' ');
	infile.seekg(oldpos);

	int linenumber = 0;
	while (std::getline(infile, line)) ++linenumber;
	infile.clear();
	infile.seekg(oldpos);
	matrix<double> dat(linenumber, cols);

	for (int i = 0; i < linenumber; i++)
	{
		std::getline(infile, line);
		stringstream ss(line);
		for (int j = 0; j < cols; j++)
		{
			ss >> tempstr;
			dat(i, j) = stod(tempstr);

		}
	}

	/*
	while (std::getline(infile, line))
	{
		stringstream ss(line);
		for(int i=0; i<cols; i++)
		{
			ss >> tempstr;
			dat(dat.size1() - 1, i)=stod(tempstr);
			cout << dat(dat.size1() - 1, i);
		}
	}
	*/
	infile.close();
	return dat;

}

void FitCreator::supdate()
{
	
	matrix<double> dt = matrixdata["s(1,1).multidim(1,1).dim(1,1).delta"];
	matrix<double> runlen = matrixdata["s(1,1).multidim(1,1).p(1,1).runlen"];
	matrix<double> dat;
	mapped_matrix<double> sparsedat;
	double summ = 0;
	int counter = 0;

	dat = matrixdata["s(1,1).multidim(1,1).var(1,1).dat"];
	if (dat.size1() == 0)
	{
		dat = sparsematrixdata["s(1,1).multidim(1,1).var(1,1).dat"];
		matrixdata["s(1,1).multidim(1,1).var(1,1).dat"] = dat;
		if (dat.size1() == 0)
			return;
	}
	else
	{
		for (int i = 0; i < dat.size1(); i++)
		{
			for (int j = 0; j < dat.size2(); j++)
			{
				if (dat(i, j) != 0)
				{
					summ += dat(i, j);
					counter++;
				}
			}
		}
		if (summ / counter < 0.01)
		{
			sparsematrixdata["s(1,1).multidim(1,1).var(1,1).dat"] = dat;
		}		
	}
	
	stringdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).type"] = "pattern";
	matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentDur"] = (matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"] - matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"] + 1) * dt(0,0);
	if (matrixdata["s(1,1).multidim(1,1).p(1,1).preludelen"].size1()>0)
	{

		//tempmatrix = matrixdata["s(1,1).multidim(1,1).p(1,1).preludelen"] * matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0);
		//tempmatrix = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"] + tempmatrix(0, 0);
		matrix<double> LoIndex(1, matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0));
		matrix<double> HiIndex(1, matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0));
		for (int i= 1; i <= matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0); i++)
		{
			LoIndex(0, i - 1) = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, i - 1) + (matrixdata["s(1,1).multidim(1,1).p(1,1).preludelen"] * i)(0,0);
			HiIndex(0, i - 1) = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"](0, i - 1) + (matrixdata["s(1,1).multidim(1,1).p(1,1).preludelen"] * i)(0, 0);		
		}
		matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentRecordFrameLo"] = LoIndex;
		matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentRecordFrameHi"] = HiIndex;
	}
	regex exp("P1|P2|P3|P3b|P3Pres|P3PresDup");
string str = stringdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).screenname"];
std::sregex_iterator iter(str.begin(), str.end(), exp);
int needfliplr;
if (iter->size() > 0)
needfliplr = 1;
else
needfliplr = 0;
matrixdata["s(1,1).multidim(1,1).p(1,1).reglag"] = matrixdata["s(1,1).multidim(1,1).p(1,1).regdelay"];
if (needfliplr)
{
	if (stringdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).bmpflip"].length() == 0)
		stringdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).bmpflip"] = "none";

	if (strcmp(stringdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).bmpflip"].c_str(), "lr") != 0)
	{
		matrix<double> xmid = matrixdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).xmid"];
		for (double val : xmid.begin1())
			val = round(1000 * val);

		matrix<double> ymid = matrixdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).ymid"];
		for (double val : ymid.begin1())
			val = round(1000 * val);

		/*
		regionfliplr = lookuprows([-xmid ymid], [xmid ymid]);

		s.p.reglag = s.p.reglag(:, regionfliplr);
		s.p.name = [s.p.name '_fliplr'];
		disp('Flipped regions LR')
		*/
	}
}
stringdata["s(1,1).multidim(1,1).p(1,1).pl(1,1).linestring"] = ".-";
string flds[3] = { "cdat","fixationdat","taskdat" };
for (string s : flds)
{
	if (matrixdata.find("s(1,1).multidim(1,1).p(1,1)." + s) != matrixdata.end())
	{
		matrixdata["s(1,1).multidim(1,1).var(1,1)." + s] = matrixdata["s(1,1).multidim(1,1).p(1,1)." + s];
	}
}
if (stringdata.find("s(1,1).multidim(1,1).p(1,1).stimtype") == stringdata.end())
{
	if (stringdata.find("s(1,1).multidim(1,1).p(1,1).type") != stringdata.end())
		stringdata["s(1,1).multidim(1,1).p(1,1).stimtype"] = stringdata["s(1,1).multidim(1,1).p(1,1).type"];
	else
		stringdata["s(1,1).multidim(1,1).p(1,1).stimtype"] = "PP";
}
if (matrixdata.find("s(1,1).multidim(1,1).p(1,1).bipolar") == matrixdata.end())
matrixdata["s(1,1).multidim(1,1).p(1,1).bipolar"](0, 0) = 0;
if (stringdata["s(1,1).multidim(1,1).p(1,1).stimtype"] == "CR" || stringdata["s(1,1).multidim(1,1).p(1,1).stimtype"] == "CRQR")
matrixdata["s(1,1).multidim(1,1).p(1,1).bipolar"](0, 0) = 1;
matrixdata["s(1,1).multidim(1,1).p(1,1).bipolar"](0, 0) = abs(matrixdata["s(1,1).multidim(1,1).p(1,1).bipolar"](0, 0) != 0);

stringdata["s(1,1).multidim(1,1).dim(2,1).name"] = "component";
stringdata["s(1,1).multidim(1,1).dim(4,1).name"] = "region";
matrixdata["s(1,1).multidim(1,1).dim(4,1).size"] = matrixdata["s(1,1).multidim(1,1).p(1,1).pattern(1,1).nreg"];

string vnames[27] = { "runlen", "FrameRound",	"pulselen",	"SegmentHiIndex",	"SegmentLoIndex",	"preludelen",	"preludelen",	"prepreludelen",	"postludelen",	"SegmentRecordFrameLo",	"SegmentRecordFrameHi",	"CycleLoIndex",	"CycleHiIndex",	"fdot",	"reglag",	"regcondlag",	"rightdelay",	"screenlag",	"regdelayrange",	"reglagDiff",	"reglagDiffMinMaxMeanMedian",	"reglagDiffMinMaxMeanMedianSec",	"reglagModfdotDiff",	"reglagmax",	"reglagmin",	"reglagmod",	"reglagonsparse" };
for (string s : vnames)
{
	if (matrixdata.find("s(1,1).multidim(1,1).p(1,1)." + s) != matrixdata.end())
		matrixdata["s(1,1).multidim(1,1).p(1,1).seconds(1,1)." + s] = matrixdata["s(1,1).multidim(1,1).p(1,1)." + s] * dt(0, 0);
	else
		matrixdata["s(1,1).multidim(1,1).p(1,1).seconds(1,1)." + s](0, 0) = 'NaN';
}
}

matrix<double> FitCreator::diffbackward(matrix<double> d)
{
	matrix<double> dif(d.size1(), d.size2());
	dif(0) = (double)NAN;

	for (int i = 0; i < d.size1() - 1; i++)
	{
		for (int j = 0; j < d.size2(); j++)
		{
			dif(i + 1, j) = d(i + 1, j) - d(i, j);
		}
	}
	return dif;
}

matrix<double> FitCreator::diffforward(matrix<double> d)
{
	matrix<double> dif(d.size1(), d.size2());
	dif(d.size1() - 1) = (double)NAN;
	for (int i = 0; i < d.size1() - 1; i++)
	{
		for (int j = 0; j < d.size2(); j++)
			dif(i, j) = d(i + 1, j) - d(i, j);
	}
	return dif;
}


//check all element from any A position in B
matrix<bool> FitCreator::ismember(matrix<double> A, matrix<double> B)
{
	matrix<double> member(A.size1(), A.size2(), 0);
	bool mainLoop = false;
	for (int i = 0; i < A.size1(); i++)
	{
		for (int j = 0; j < A.size2(); j++)
		{
			mainLoop = false;
			for (int row = 0; row < B.size1() && !mainLoop; row++)
			{
				for (int col = 0;col < B.size2() && !mainLoop; col++)
				{
					if (A(i, j) == B(row, col))
					{
						A(i, j) = 1;
						mainLoop = true;
					}

				}
			}
		}
	}
	return member;
}

matrix<double> FitCreator::filter(const boost::numeric::ublas::vector<double> b, const matrix<double> a,const matrix<double> x)
{
	int ord = b.size() - 1;
	int np = x.size1() - 1;
	matrix<double> y(x.size1(), x.size2());
	y(0, 0) = b(0)*x(0, 0);
	for (int i = 1; i < ord+1; i++)
	{
		y(i, 0) = 0;
		for (int j = 0; j < i+1;j++)
		{
			y(i, 0) += b(j)*x(i - j);
		}
		for (int j = 0;j <i; j++)
		{	
			y(i, 0) -= a(j + 1, 0)*y(i - j - 1);
		}
	}
	for (int i = ord+1; i < np+1; i++)
	{
		y(i, 0) = 0;
		for (int j = 0; j < ord+1; j++)
		{
			y(i, 0) += b(j)*x(i - j);
		}
		for (int j = 0; j < ord; j++)
		{
			y(i, 0) -= a(j + 1, 0)*y(i - j - 1);
		}
	}
	return y;
}

matrix<double> FitCreator::sum(matrix<double> x)
{
	matrix<double> s(1, x.size2(),0);
	for (int i = 0; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)
			s(0, j) += x(i, j);
	return s;
}

//Make nuisance parameter design matrix
void FitCreator::estxnmake()
{
	double TrendOrderPlusOne;
	if (!isnan(matrixdata["fitoptions(1,1).TrendOrder"](0,0)))
		TrendOrderPlusOne = matrixdata["fitoptions(1,1).TrendOrder"](0, 0) + 1;
	else
		TrendOrderPlusOne = 0;

	//humorder always 0 by optionfile

	matrix<double> nkn = matrixdata["r(1,1).p(1,1).SegmentNum"] * (TrendOrderPlusOne + matrixdata["fitoptions(1,1).HumOrder"](0, 0) * 2) + (matrixdata["est.samperstim"] - 1);
	matrix<double> vardat (matrixdata["r(1,1).var(1,1).dat"].size1(), matrixdata["r(1,1).p(1,1).SegmentNum"](0,0), 0);
	double klast;
	if (matrixdata["fitoptions(1,1).TrendOrder"](0, 0) >= 0 || matrixdata["fitoptions(1,1).HumOrder"](0, 0) > 1)
	{
		matrix<double>Xnseg;

		for (int seg = 0; seg < matrixdata["r(1,1).p(1,1).SegmentNum"](0, 0); seg++)
		{
			int k = seg+1;
			int size = matrixdata["r(1,1).p(1,1).SegmentHiIndex"](0,seg) - matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0,seg) + 1;
			matrix<double> i(size, 1, 0);
			for (int iter = 0; iter < i.size1(); iter++)
				i(iter, 0) = matrixdata["r(1,1).p(1,1).SegmentLoIndex"](0,seg) + iter-1;
			for (int trendOrder = 0; trendOrder <= matrixdata["fitoptions(1,1).TrendOrder"](0, 0); trendOrder++)
			{
				matrix<double> xseg;
				if (trendOrder == 0)
				{
					xseg = matrix<double>(size, 1, 1);
				}
				else
				{
					xseg = (i - mean(i))*matrixdata["r(1,1).p(1,1).delta"](0, 0) / 60; // time in minutes
					xseg = pow(xseg, trendOrder);
					xseg = xseg -(Xnseg *(Xnseg / xseg));
					
				}
				int oldsize = Xnseg.size1();
				Xnseg.resize(Xnseg.size1() + xseg.size1(), 1);
				int j = 0;
				for (int iter = oldsize; iter < Xnseg.size1(); iter++)
				{
					Xnseg(iter, 0) = xseg(j, 0);
					j++;
				}

				for (int iter = 0; iter < i.size1(); iter++)
				{
					vardat(i(iter), k-1) = xseg(iter, 0);
				}

				k += matrixdata["r(1,1).p(1,1).SegmentNum"](0, 0);
			}

			for (int humorder = 1; humorder < matrixdata["fitoptions(1,1).HumOrder"](0, 0); humorder++)
			{
				//never used code 4337
			}

			klast = matrixdata["r(1,1).p(1,1).SegmentNum"](0, 0) *(TrendOrderPlusOne+matrixdata["fitoptions(1,1).HumOrder"](0, 0) * 2);	
		}
	}
	matrixdata["est.XnScalar"] = matrix<double>(1,1,1000);
	matrixdata["est.x.Xn.var.dat"] = vardat * matrixdata["est.XnScalar"](0,0);
	matrixdata["est.nkn"] =matrix<double> (1,1,klast);
	matrixdata["est.nbn"] = matrix<double>(1, 1, klast);	
}

void FitCreator::ypreprocess7()
{
	matrixdata["r(1,1).var(1,1).Outlier"] = matrix<double>(matrixdata["r(1,1).var(1,1).dat"].size1(), matrixdata["r(1,1).var(1,1).dat"].size2(), 0);
	matrixdata["r(1,1).var(1,1).datRawMedian"] = matrix<double>(1, matrixdata["r(1,1).var(1,1).dat"].size2(), (double)NAN);
	boost::numeric::ublas::vector<double> column(matrixdata["r(1,1).var(1,1).dat"].size1());

	for (int j = 0; j < matrixdata["r(1,1).var(1,1).dat"].size2(); j++)
	{	
		matrix_column<matrix<double>> col(matrixdata["r(1,1).var(1,1).dat"], j);
		copy(col.begin(), col.end(), column.begin());
		matrixdata["r(1,1).var(1,1).datRawMedian"](0, j) = mediannotnan(col);
		if (matrixdata["fitoptions(1,1).OutlierRelMedianHiLim"](0, 0) < INFINITE)
		{
			for (int i = 0; i < matrixdata["r(1,1).var(1,1).dat"].size1(); i++)
				matrixdata["r(1,1).var(1,1).Outlier"](i, j) = matrixdata["r(1,1).var(1,1).dat"](i, j) > matrixdata["r(1,1).var(1,1).datRawMedian"](0, j) + matrixdata["fitoptions(1,1).OutlierRelMedianHiLim"](0, 0);
		}		
	}
	matrixdata["r(1,1).var(1,1).OutlierRelMedianHiNum"] = sum(matrixdata["r(1,1).var(1,1).Outlier"]);
	if (matrixdata["fitoptions(1,1).ExcludedCommon"](0, 0))
	{
		matrix<bool> b = any(matrixdata["r(1,1).var(1,1).Outlier"]);	
		for (int i = 0; i < matrixdata["r(1,1).var(1,1).Outlier"].size1(); i++)
			if (b(i, 0) == 1)
			{
				for (int j = 0; j < matrixdata["r(1,1).var(1,1).Outlier"].size1(); j++)
				{
					matrixdata["r(1,1).var(1,1).Outlier"](i, j) = 1;
					matrixdata["r(1,1).var(1,1).dat"](i, j) = 'NaN';
				}

			}
	}
	matrixdata["r(1,1).var(1,1).OutlierNum"] = sum(matrixdata["r(1,1).var(1,1).Outlier"]);
	matrix<bool> n = isnan(matrixdata["r(1,1).var(1,1).dat"]);
	for (int i = 0; i < matrixdata["r(1,1).var(1,1).Missing"].size1(); i++)
	{
		for (int j = 0; j < matrixdata["r(1,1).var(1,1).Missing"].size2(); j++)
		{
			if (n(i, j) == 1)
				matrixdata["r(1,1).var(1,1).Missing"](i, j) = n(i, j);
		}
	}
	matrixdata["r(1,1).var(1,1).MissingNum"] = sum(matrixdata["r(1,1).var(1,1).Missing"]);

	matrix <double> s = sum(matrixdata["r(1,1).var(1,1).dat"]);
	if (isnan(s(0, 0)) || isnan(s(0, 1)))	//any NaN in y.var.dat
	//if (s(0,0)=='NaN' || s(0,1) =='NaN')	//any NaN in y.var.dat
	{
		for (int j = 0; j < matrixdata["r(1,1).var(1,1).dat"].size2(); j++)
		{
			matrix_column<matrix<double>> yj(matrixdata["r(1,1).var(1,1).dat"], j);
			for (int seg = 0; seg < matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentNum"](0, 0); seg++)
			{
				 int len = matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"](0, seg) - matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, seg) + 1;
				 matrix<double> yjseg(len, 1, 0);
				 matrix_column<matrix<double>> yjsegcolumn(yjseg, 0);
				 copy(yj.begin() + matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, seg) - 1, yj.begin() + matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"](0, seg), yjsegcolumn.begin());
				//copy(yj(matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, seg) + 1), yj(matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentHiIndex"](0, seg) + 1), yjseg.begin1());
				// for (int iter = 0; iter < yjseg.size1(); iter++)
				//	 cout <<iter<<' '<< yjseg(iter,0) << endl;
				 matrix<double> igood = find(!isnan(yjseg));
				 if (igood.size1()==0)
				 {
					 double val = mediannotnan(yj);
					 if (val=='NaN')
						 val = 3500;
					for (int iter = 0; iter < yjseg.size1(); iter++)
					{
						yjseg(iter) = val;
					}

				 }
				 else
				 {
					 for (int iter = 0; iter < igood(0, 0); iter++)
						 yjseg(iter) = yjseg(igood(0));
					 for (int iter = igood(igood.size1()-1); iter < yjseg.size1(); iter++)
						 yjseg(iter) = yjseg(igood(igood.size1() - 1));
					 matrix<double> ibad = isnan(yjseg);
					 matrix<double> ibadDiff = diff(ibad);				 
					 matrix<double> ibadStart = find(ibadDiff == 1);
					 matrix<double> ibadEnd = find(ibadDiff == -1)+1;
					 matrix<double> iEndPoints = matrix<double>(ibadStart.size1() + ibadEnd.size1(), 1);
					 for (int iter = 0; iter < ibadStart.size1(); iter ++)
					 {
						 iEndPoints(iter * 2) = ibadStart(iter)+1;
						 iEndPoints(iter * 2+1) = ibadEnd(iter)+1;
					 }
					 unique(iEndPoints.begin1(), iEndPoints.end1());	
					 matrix<double> s = sum(ibad);
					 if (sum(ibad)(0, 0) > 0)
					 {
						
						 ibad = find(ibad)+1;
						 matrix<double> v(iEndPoints.size1(), 1);
						 for (int i = 0; i < v.size1(); i++)
						 {
							 v(i, 0) = yjseg(iEndPoints(i, 0)-1);
						 }
						 matrix<double> interpvalue(ibad.size1(), 1);
						 for (int i = 0; i < ibad.size1(); i++)
						 {
							 interpvalue(i) = interpolate(iEndPoints, v, ibad(i));
						 }
						 for (int i = 0; i < interpvalue.size1(); i++)
							 yjseg(ibad(i)-1) = interpvalue(i);

					 }
				 }		
				 for (int iter = 0; iter < yjseg.size1(); iter++)
					 matrixdata["r(1,1).var(1,1).dat"](matrixdata["s(1,1).multidim(1,1).p(1,1).SegmentLoIndex"](0, seg)-1+iter, j) = yjseg(iter, 0);
		
			}
			
		}
		
	}
	
	matrixdata["r(1,1).var(1,1).MissingNum"] = sum(matrixdata["r(1,1).var(1,1).Missing"]);
	matrixdata["r(1,1).var(1,1).MissingPerc"] = matrixdata["r(1,1).var(1,1).MissingNum"] / matrixdata["r(1,1).var(1,1).dat"].size1() * 100;
}

matrix<double> FitCreator::delay(const matrix<double> x)
{
	matrix<double> y(x.size1(), x.size2(),0);
	for (int i = 1; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)
			y(i, j) = x(i - 1, j);
	return y;
}

matrix<double> FitCreator::delay(const matrix<double> x, const matrix<double> k)
{

	matrix<double> y(x.size1(), k.size1(), 0);
	for (int j = 0; j < y.size2(); j++)
		for (int i = k(j), xpos = 0; i < y.size1(); i++)
			y(i, j) = x(xpos++,0);
	return y;
}

matrix<double> FitCreator::delaycy(const matrix<double> x, matrix<double> lag)
{
	int n = x.size1();
	lag = lag - floor(lag / n) * n;
	int xncol = x.size2();
	int nlag = lag.size2();
	matrix<double> y(n, lag.size2()*x.size2(),0);
	matrix<double> iy(1, n);
	for (int jlag = 0; jlag < nlag; jlag++)
	{
		for (int i = 0;  lag(jlag) + i <n; i++)
		{
			iy(0, i) = lag(jlag) + i;
		}
		for (int i = n - lag(jlag), j=0; i < n; i++, j++)
		{
			iy(0, i) = (double)j;
		}
		for (int i = 0; i < y.size1(); i++)	
			for (int j = 0; j < xncol; j++)
				y(iy(0, i), jlag*xncol+j) = x(i,j);	
	}
	return y;
}

matrix<double> FitCreator::delaycy(const matrix<double> x, matrix<double> lag, matrix<double> i)
{
	int n = x.size1();
	int nlag = lag.size2();
	matrix<double> y(i.size1() * i.size2(), x.size2() * nlag, 0);
	matrix<double> I(y.size1(), 1);
	int cnt = 0;
	for (int col = 0; col < i.size2(); col++)
		for (int row = 0; row < i.size1(); row++)
		{
			I(cnt) = i(row, col);
			cnt++;
		}
	matrix<double> ix(y.size1(), 1);
	
	for (int jlag = 0; jlag < nlag; jlag++)
	{
		for (int r = 0; r < ix.size1(); r++)
		{
			ix(r) = I(r) - lag(jlag);
			ix(r) = ix(r) - floor(ix(r)/ n)* n;
		}
		for (int iter = 0; iter < y.size1(); iter++)
		{
			for (int col = 0; col < x.size2(); col++)
				y(iter, jlag * x.size2() + col) = x(ix(iter), col);

		}
	}
	return y;
}

matrix<double> FitCreator::trtimesminus(matrix<double> x, matrix<double> y, matrix<double> ibad)
{ 
	matrix<double> z;
	if (y.size1() == 0)
	{
		z = prod(trans(x), x);
		if (ibad.size1() != 0)
		{
			matrix<double> xbad(ibad.size1(), x.size2());
			for(int i=0; i<xbad.size1(); i++)
				for (int j = 0; j < xbad.size2(); j++)
					xbad(i, j) = x(ibad(i), j);
			z = z - prod(trans(xbad), xbad);	
		}

	}
	else
	{
		z = prod(trans(x), y);
		if (ibad.size1() != 0)
		{
			matrix<double> xbad(ibad.size1(), x.size2());
			for (int i = 0; i < xbad.size1(); i++)
				for (int j = 0; j < xbad.size2(); j++)
					xbad(i, j) = x(ibad(i), j);
			matrix<double> ybad(ibad.size1(), y.size2());
			for (int i = 0; i < ybad.size1(); i++)
				for (int j = 0; j < ybad.size2(); j++)
					ybad(i, j) = y(ibad(i), j);
			z = z - prod(trans(xbad), ybad);
		}
	}
	return z;
}


void FitCreator::readtxt(string filename)
{

	boost::filesystem::ifstream myfile(filename);
	string name, type, value;
	double val;
	string str;
	int m, n; //rows and columns number
	if (myfile.is_open())
	{
		bool status = true;
		std::getline(myfile, name);
		while (!myfile.eof())
		{
			name = name.substr(1);

			//get type
			std::getline(myfile, type);

			std::getline(myfile, value);
			if (type == "char")
			{
				stringdata.insert({ name, value });
				std::getline(myfile, name);
			}
			else
				if (type == "sparse")
				{
					str.clear();
					/*
					while (value[0] != '%' && !myfile.eof())
					{
						str += value + ";";
						getline(myfile, value);
					}
					mymap.insert({ name, str });
					*/
					smatch res;
					regex exp("\-?[[:digit:]]+");
					string::const_iterator searchStart;
					m = stoi(value);
					std::getline(myfile, value);
					n = stoi(value);
					std::getline(myfile, value);

					std::vector<std::vector<double>> tmp;		//temp for sparse matrix (resizing doesnot work for them);
					while (value[0] != '%' && !myfile.eof())
					{				
						std::vector<double> numbers;
						std::sregex_iterator iter(value.begin(), value.end(), exp);
						std::sregex_iterator end;
						while (iter != end)
						{
							for (unsigned i = 0; i < iter->size(); ++i)
							{
								numbers.push_back(stod((*iter)[i]));
							}
							++iter;
						}
						tmp.push_back(numbers);	
						if (numbers.size() <= 1)
						{
							numbers.clear();
							tmp.clear();						
							break;
						}
						numbers.clear();
						std::getline(myfile, value);
					}
					if (tmp.size() != 0)
					{
						mapped_matrix <double> tsp(m, n, tmp.size());		//temp sparse matrix
						for (std::vector<double> v : tmp)
						{
							tsp.insert_element(v[0] - 1, v[1] - 1, v[2]);
						}
						sparsematrixdata.insert({ name, tsp });
					}
					else
					{
						mapped_matrix <double> tsp(m, n);
						sparsematrixdata.insert({ name, tsp });
					}
					
					tmp.clear();				
					name = value;
				}
				else			
					//!!!warning!!! only 2-d matrix
					if (type == "field")
					{
						
						std::vector<string> splitstring;
						split(value, splitstring, ' ');
						m = atoi(splitstring[0].c_str());
						n = atoi(splitstring[1].c_str());
						str.clear();
						tempmatrix.clear();
						tempmatrix.resize(m, n);

						int line, column;
						line = 0;

						std::getline(myfile, value);
						while (value[0] != '%' && !myfile.eof())
						{
							splitstring.clear();
							split(value, splitstring, ' ');		
	
							for (column  = 0; column < n; column++)					
								tempmatrix.insert_element(line, column, stod(splitstring[column]));
							line++;
							//str += value + ";";
							std::getline(myfile, value);
						}

						matrixdata.insert({ name, tempmatrix });
						//mymap.insert({ name, str });
						name = value;
					}
					else
					{
						while (value[0] != '%' && !myfile.eof())
						{
							std::getline(myfile, value);
						}
						stringdata.insert({ name, value });
						name = value;
					}
		}
		tempmatrix.clear();
		myfile.close();
	}
	else
	{
		return;
	}

}

unsigned int FitCreator::split(std::string &txt, std::vector<std::string> &strs, char ch)
{
	unsigned int pos = txt.find(ch);
	unsigned int initialPos = 0;
	strs.clear();

	// Decompose statement
	while (pos != std::string::npos) {
		pos--;
		strs.push_back(txt.substr(initialPos, pos - initialPos+1));

		initialPos = pos + 2;

		pos = txt.find(ch, initialPos);
	}
	// Add the last one
	strs.push_back(txt.substr(initialPos, (pos < txt.size() ? pos : txt.size()) - initialPos + 1));

	return strs.size();

}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }
void FitCreator::removeSpaces(std::string& str)
{
	std::string::iterator new_end = std::unique(str.begin(), str.end(), BothAreSpaces);
	str.erase(new_end, str.end());

}


//transform string from view [1][1]=10 into matrix
void FitCreator::strtomatrix(string str, matrix <double> *mat)
{
	size_t found1 = -1;
	size_t found2 = -1;
	int number;
	string temp;
	std::vector <int> ElementPosition;
	while(true)
	{
		found1 = str.find_first_of("[", found1+1);
		found2 = str.find_first_of("]", found2+1);
		if (found1 == string::npos || found2 == string::npos)
			break;
		else
		{
			temp = str.substr(found1 + 1, found2 - 1);
			number = stoi(temp);
			ElementPosition.push_back(number);
		}
	}
	if (ElementPosition.size() == 2)
	{
		/*
		size_t newsize1, newsize2;
		newsize1 = tempmatrix.size1() < ElementPosition[0] ? ElementPosition[0] : tempmatrix.size1();
		newsize2 = tempmatrix.size2() < ElementPosition[1] ? ElementPosition[1] : tempmatrix.size2();
		tempmatrix.resize(newsize1, newsize2);
		*/

		int equalpos = str.find_first_of('=');
		temp = str.substr(equalpos + 1, str.length() - 1);
		double value = stod(temp);
		mat->insert_element(ElementPosition[0] - 1, ElementPosition[1] - 1, value);
	}

}
