/*
    Fast Fourier Transformation
    ====================================================
    Coded by Miroslav Voinarovsky, 2002
    This source is freeware.
*/

#ifndef FFT_H_
#define FFT_H_

struct Complex;

/*
  Fast Fourier Transformation
  x: x - array of items
  N: N - number of items in array
  complement: false - normal (direct) transformation, true - reverse transformation
*/
extern void universal_fft(Complex* x, int N, bool complement);

struct Complex
{
    long double re, im;
    inline void operator= (const Complex& y);
};

inline void Complex::operator= (const Complex& y) { re = y.re; im = y.im; }

#endif