#include "stdafx.h"
#include "OperatorOverload.h"


matrix<double> operator +(const matrix<double> x1, const double x2)
{
	matrix<double> sum(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			sum(i, j) = x1(i, j) + x2;
	return sum;
}

matrix<double> operator +(const double x2, const matrix<double> x1)
{
	matrix<double> sum(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			sum(i, j) = x1(i, j) + x2;
	return sum;
}
matrix<double> operator -(const matrix<double> x1, const double x2)
{
	matrix<double> sum(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			sum(i, j) = x1(i, j) - x2;
	return sum;
}


matrix<double> operator *(const matrix<double> x1, const matrix<double> x2)
{
	matrix<double> val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = x1(i, j)*x2(i, j);
	return val;
}

matrix<double>REPORTER_API operator/(const matrix<double> x1, const matrix<double> x2)
{
	matrix<double> val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = x1(i, j)/x2(i, j);
	return val;
}


matrix<double> Max(int A, const matrix<double> B)
{
	matrix<double> val(B.size1(), B.size2());
	for (int i = 0; i < B.size1(); i++)
		for (int j = 0; j < B.size2(); j++)
			val(i, j) = max(A, B(i, j));
	return val;
}

double REPORTER_API Max(const matrix<double> A)
{
	double val = A(0, 0);
	for (int i = 0; i < A.size1(); i++)
		for (int j = 0; j < A.size2(); j++)
			val = max(val, A(i, j));
	return val;
}

matrix<double>log(const matrix<double> x)
{
	matrix<double> val(x.size1(), x.size2());
	for (int i = 0; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)
			val(i, j) = log(x(i, j));
	return val;
}

matrix<double> pow(const matrix<double> x1, double x2)
{
	matrix<double> val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = pow(x1(i, j), x2);
	return val;
}

matrix<double> exp(const matrix<double> x)
{
	matrix<double> val(x.size1(), x.size2());
	for (int i = 0; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)
			val(i, j) = exp(x(i, j));
	return val;
}

matrix<double> ceil(const matrix<double> x1)
{
	matrix<double>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = ceil(x1(i, j));
	return val;
}

matrix<double> floor(const matrix<double> x1)
{
	matrix<double>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = floor(x1(i, j));
	return val;
}

matrix<double>REPORTER_API mean(const matrix<double> x)
{
	matrix<double>m (1, x.size2(),0);
	for (int j = 0; j < x.size2(); j++)
	{
		for (int i = 0; i < x.size1(); i++)
			m(0, j) += x(i, j);
		m(0, j) = m(0, j) / x.size1();
	}
	return m;
}

auto cmp = [](const double& d1, const double& d2) {
	if (isnan(d1)) return false;
	if (isnan(d2)) return true;
	return d1 < d2;
};
double REPORTER_API mediannotnan(const boost::numeric::ublas::vector<double> x)
{
	double med;
	std::vector<double> Xsort(x.size());
	copy(x.begin(), x.end(), Xsort.begin()); 
	std::sort(Xsort.begin(), Xsort.end(), cmp);
	//int Count = Xsort.size() - boost::range::count(x, 'NaN');
	int Count = 0;
	for (int i = 0; i < Xsort.size(); i++)
	{
		if (!isnan(Xsort[i]))
			Count++;
	}

	float imid = (Count + 1) / 2;
	imid = max(1, imid);
	int i1 = floor(imid);
	int i2 = ceil(imid);
	med = (Xsort[i1] + Xsort[i2]) / 2;
	return med;
}

matrix<bool>REPORTER_API any(const matrix<bool> x)
{
	matrix<bool>a(x.size1(), 1, 0);
	if (x.size2() >= 2)
	{
		for (int i = 0; i < x.size1(); i++)
		{
			a(i, 0) = x(i, 0);
			for (int j = 1; j < x.size2(); j++)
			{
				a(i, 0) = a(i, 0) | x(i, j);
			}
		}
		return a;
	}
	else
	{
		return x;
	}

}

matrix<double> diff(const matrix<double> d)
{
	matrix<double> dif(d.size1() - 1, d.size2());
	for (int i = 0; i < d.size1() - 1; i++)
	{
		for (int j = 0; j < d.size2(); j++)
			dif(i, j) = d(i + 1, j) - d(i, j);
	}
	return dif;
}

matrix<double>REPORTER_API find(const matrix<double> d)
{
	std::vector<int> vect;
	for (int j = 0; j < d.size2(); j++)
	{
		for (int i = 0; i < d.size1(); i++)
		if (d(i,j) != 0)
			vect.push_back(i+j*d.size1());
	}
	matrix<double> val(vect.size(), 1);
	copy(vect.begin(), vect.end(), val.begin1());
	return val;
}

matrix<bool>REPORTER_API isnan(const matrix<double> x)
{
	matrix<bool> val(x.size1(), x.size2());
	for (int i = 0; i < x.size1(); i++)
	{
		for (int j = 0; j < x.size2(); j++)
			val(i, j) = x(i, j)=='NaN' || isnan(x(i,j)) || x(i,j)== (double)NAN ;
	}
	return val;
}



double interpolate(const matrix<double> xData, const matrix<double> yData, double x)
{
	int size = xData.size1();

	int i = 0;                                                                  // find left end of interval for interpolation
	if (x >= xData(size - 2))                                                 // special case: beyond right end
	{
		i = size - 2;
	}
	else
	{
		while (x > xData(i + 1)) i++;
	}
	double xL = xData(i), yL = yData(i), xR = xData(i + 1), yR = yData(i + 1);      // points on either side (unless beyond ends)

	if (x < xL) yR = yL;
	if (x > xR) yL = yR;

	double dydx = (yR - yL) / (xR - xL);                                    // gradient

	return yL + dydx * (x - xL);                                              // linear interpolation
}

matrix<double> chol(const matrix<double> A)
{
	int n = A.size1();
	matrix<double> lower(A.size1(),A.size2(),0);

	// Decomposing a matrix into Lower Triangular 
	for (int i = 0; i < n; i++) 
	{
		for (int j = 0; j <= i; j++) 
		{
			double sum = 0;
			if (j == i) // summation for diagnols 
			{
				for (int k = 0; k < j; k++)
					sum += pow(lower(j,k), 2);
				lower(j,j) = sqrt(A(j,j) -
					sum);
			}
			else {
				// Evaluating L(i, j) using L(j, j) 
				for (int k = 0; k < j; k++)
					sum += (lower(i,k) * lower(j,k));
				lower(i,j)= (A(i,j) - sum) /
					lower(j,j);
			}
		}
	}
	return trans(lower);
}

matrix<double> speye(int n)
{
	matrix<double> val(n, n, 0);
	for (int i = 0; i < n; i++)
		val(i, i) = 1;
	return val;
}

matrix<double> kron(matrix<double> A, matrix<double> B)
{
	matrix<double> val(A.size1() * B.size1(), A.size2() * B.size2(),0);
	for (int i = 0; i < A.size1(); i++) {
		for (int j = 0; j < A.size2(); j++) {
			int startRow = i * B.size1();
			int startCol = j * B.size2();
			for (int k = 0; k < B.size1(); k++) {
				for (int l = 0; l < B.size2(); l++) {
					val(startRow + k, startCol + l) = A(i, j) * B(k, l);
				}
			}
		}
	}
	return val;
}

matrix<double> reshape(const matrix<double> A, int r, int c)
{

	int x = A.size1();
	int y = A.size2();
	if (x * y != r * c)
		return A;
	else
	{
		matrix<double> val(r, c);
		int cnt=0;
		for (int col = 0; col < y; ++col)
			for (int row = 0; row < x; ++row)
			{
				val(cnt % r, cnt / r) = A(row, col);
				cnt++;
			}

		return val;
	}
	//int cnt = 0;
	//for (int row = 0; row < x; ++row) 
	//{
	//	for (int col = 0; col < y; ++col) 
	//	{
	//		val(cnt / c, cnt % c) = A(row, col);
	//		cnt++;
	//	}
	//}
}

matrix<std::complex<double>> fft(matrix<std::complex<double>> A, int N)
{
	matrix<std::complex<double>>val(N, A.size2());
	Complex *In=new Complex[N];
	for (int j = 0; j < A.size2(); j++)
	{
		for (int i = 0; i < A.size1(); i++)
		{
			In[i].re = (long double)A(i, j).real();
			In[i].im = A(i, j).imag();
		}
		if (A.size1() < N)
		{
			for (int i = A.size1(); i < N; i++)
			{
				In[i].re = 0; In[i].im = 0;
			}
		}
		universal_fft(In, N, false);
		for (int i = 0; i < N; i++)
			val(i, j) = std::complex<double>(In[i].re, In[i].im);
	}
	return val;
}

matrix<std::complex<double>> ifft(matrix<std::complex<double>> A, int N)
{
	matrix<std::complex<double>>val(N, A.size2());
	Complex* In = new Complex[N];
	for (int j = 0; j < A.size2(); j++)
	{
		for (int i = 0; i < A.size1(); i++)
		{
			In[i].re = (long double)A(i, j).real();
			In[i].im = A(i, j).imag();
		}
		if (A.size1() < N)
		{
			for (int i = A.size1(); i < N; i++)
			{
				In[i].re = 0; In[i].im = 0;
			}

		}
		universal_fft(In, N, true);

		for (int i = 0; i < N; i++)
			val(i, j) = std::complex<double>(In[i].re, In[i].im);
	}
	return val;
}





matrix<std::complex<double>>REPORTER_API roots(matrix<double> x)
{
	int size = x.size1();
	int n = size-1;
	double* a = new double[n + 1];
	double* x1 = new double[n + 1];
	double* wr = new double[n + 1];
	double* wi = new double[n + 1];
	for (int i = 0; i <= n; i++)
		a[i] = x(i);
	double quad[2];
	int numr;
	// initialize estimate for 1st root pair 
	quad[0] = 2.71828e-1;
	quad[1] = 3.14159e-1;
	// get roots
	get_quads(a, n, quad, x1);
	numr = findroots(x1, n, wr, wi);
	matrix<std::complex<double>> val(n,1);
	for (int i = 0; i < n; i++)
		val(i, 0) = std::complex<double>(wr[i], wi[i]);
	return val;
}

matrix<double> REPORTER_API abs(const matrix<std::complex<double>> x)
{
	matrix<double>val(x.size1(), x.size2());
	for (int i = 0; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)	
			val(i, j) = abs(x(i, j));	
	return val;
}

void REPORTER_API fill(matrix<double>* x, const double value)
{
	for (int i = 0; i < x->size1(); i++)
		for (int j = 0; j<x->size2(); j++)
			x->at_element(i,j) = value;
}


/*
std::ostream& operator<< (std::ostream &out, const matrix<double> x)
{
	for (int i = 0; i < x.size1(); i++)
	{
		for (int j = 0; j < x.size2(); j++)
			out << x(i, j) << " ";
		out << " ";
	}

	return out;
}
*/
matrix<bool> operator==(const matrix<double> x1, const double x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = x1(i, j) == x2;
	return val;
}

matrix<bool> operator>(const matrix<double> x1, const double x2)
{
	matrix<double>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = ((floor(x1(i, j)*1000)/1000) > x2);
	return val;
}

matrix<bool> operator<(const matrix<double> x1, const double x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = ((floor(x1(i, j) * 1000) / 1000) < x2);
	return val;
}

matrix<bool>REPORTER_API operator>(const double x1, const matrix<double> x2)
{
	matrix<double>val(x2.size1(), x2.size2());
	for (int i = 0; i < x2.size1(); i++)
		for (int j = 0; j < x2.size2(); j++)
			val(i, j) = (x1>(floor(x2(i, j) * 1000) / 1000));
	return val;
}

matrix<bool>REPORTER_API operator<(const double x1, const matrix<double> x2)
{
	matrix<bool>val(x2.size1(), x2.size2());
	for (int i = 0; i < x2.size1(); i++)
		for (int j = 0; j < x2.size2(); j++)
			val(i, j) = (x1 < (floor(x2(i, j) * 1000) / 1000));
	return val;
}

matrix<bool>REPORTER_API operator>=(const matrix<double> x1, const double x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = ((floor(x1(i, j) * 1000) / 1000) >= x2);
	return val;
}

matrix<bool>REPORTER_API operator<=(const matrix<double> x1, const double x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = ((floor(x1(i, j) * 1000) / 1000) <= x2);
	return val;
}

matrix<bool>REPORTER_API operator>=(const double x1, const matrix<double> x2)
{
	matrix<bool>val(x2.size1(), x2.size2());
	for (int i = 0; i < x2.size1(); i++)
		for (int j = 0; j < x2.size2(); j++)
			val(i, j) = (x1 >= (floor(x2(i, j) * 1000) / 1000));
	return val;
}

matrix<bool>REPORTER_API operator<=(const double x1, const matrix<double> x2)
{
	matrix<bool>val(x2.size1(), x2.size2());
	for (int i = 0; i < x2.size1(); i++)
		for (int j = 0; j < x2.size2(); j++)
			val(i, j) = (x1 <= (floor(x2(i, j) * 1000) / 1000));
	return val;
}

matrix<bool> operator |(const matrix<bool> x1, const matrix<bool> x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = (x1(i, j) | x2(i,j));
	return val;
}

matrix<bool>REPORTER_API operator&(const matrix<bool> x1, const matrix<bool> x2)
{
	matrix<bool>val(x1.size1(), x1.size2());
	for (int i = 0; i < x1.size1(); i++)
		for (int j = 0; j < x1.size2(); j++)
			val(i, j) = (x1(i, j) & x2(i, j));
	return val;
}

matrix<bool>REPORTER_API operator !(const matrix<bool> x)
{
	matrix<bool>val(x.size1(), x.size2());
	for (int i = 0; i < x.size1(); i++)
		for (int j = 0; j < x.size2(); j++)
			val(i, j) = !x(i, j);
	return val;
}
