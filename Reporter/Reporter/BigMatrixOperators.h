#pragma once
#include <boost/numeric/ublas/matrix.hpp>
#include "boost/numeric/ublas/matrix_proxy.hpp"
#include<boost/thread/thread.hpp>
#include<boost/bind.hpp>

using namespace boost::numeric::ublas;

namespace BigMatrixOperators
{
	//double summator(matrix<double>& a, matrix<double>& b, int i, int j);
	matrix<double> prod(matrix<double>& a,matrix<double>& b);
}