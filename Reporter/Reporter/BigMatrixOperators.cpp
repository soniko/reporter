#include "stdafx.h"
#include "BigMatrixOperators.h"

void multiply(int slice, matrix<double>& a, matrix<double>& b, matrix<double>& val)
{	 
	int SIZE = val.size1();
    const int from = (slice * SIZE) / boost::thread::hardware_concurrency();
    const int to = ((slice + 1) * SIZE) / boost::thread::hardware_concurrency();

    for (int i = from; i < to; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            for (int k = 0; k < a.size2(); k++)
				val(i,j) += a(i,k) * b(k,j);
        }
    }
}
//
//double summator(matrix<double>& a, matrix<double>& b, int i, int j)
//{
//	double val = 0;
//	for (int counter = 0; counter < a.size1(); counter++)
//		val += a(i, counter) * b(counter, j);
//	return val;
//}

matrix<double> BigMatrixOperators::prod(matrix<double>& a, matrix<double>& b)
{
	matrix<double> val(a.size1(), b.size2(),0);
	boost::thread_group threads;
	int num_thread = boost::thread::hardware_concurrency();
	for (int i = 0; i < num_thread; i++)
        {
			threads.create_thread(boost::bind(&multiply, i, boost::ref(a), boost::ref(b), boost::ref(val)));
		}
	threads.join_all();
	return val;
}