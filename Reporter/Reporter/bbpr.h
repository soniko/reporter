#pragma once
int findroots(double* a, int n, double* wr, double* wi);
void get_quads(double* a, int n, double* quad, double* x);
void recurse(double* a, int n, double* b, int m, double* quad,  double* err, int* iter);
void diff_poly(double* a, int n, double* b);
void find_quad(double* a, int n, double* b, double* quad, double* err, int* iter);
void deflate(double* a, int n, double* b, double* quad, double* err);
