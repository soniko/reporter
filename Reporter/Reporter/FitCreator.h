#pragma once
#ifdef REPORTER_EXPORTS
#define REPORTER_API __declspec(dllexport)
#else
#define REPORTER_API __declspec(dllimport)
#endif
#include <string>
#include "boost/numeric/ublas/matrix.hpp"
#include "boost/numeric/ublas/matrix_sparse.hpp"
#include "boost/numeric/ublas/io.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/numeric/ublas/matrix_proxy.hpp"
#include "boost/math/constants/constants.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/numeric/ublas/lu.hpp"
#include "boost/math/tools/roots.hpp"
#include "boost/multiprecision/cpp_dec_float.hpp"
#include <iostream>
#include <fstream>
#include <regex>
#include <fstream>
#include <iostream>
#include "OperatorOverload.h"
#include "BigMatrixOperators.h"

using namespace std;	
using namespace boost::numeric::ublas;
using namespace boost::posix_time;
using namespace boost::filesystem; 
using boost::multiprecision::cpp_dec_float_50;
#define _USE_MATH_DEFINES

class REPORTER_API FitCreator
{

private:
	string RespFile;
	string StimFile;
	string FitFile;
	matrix <double> tempmatrix;
	matrix <double> datextra;
public:

	map <string, mapped_matrix<double> > sparsematrixdata;
	map <string, matrix<double> > matrixdata;
	map <string, string> stringdata;

public:
	FitCreator();
	~FitCreator();
	void basisfun5();
	void fitbasis21();
	void mfresample(double Fs);
	matrix <double> resample(matrix <double> m, int fac);
	bool Create(string Respfile, string Stimfile, string Fitfile);
	void pupilread();
	void fitcalc();
	void ygoodsegfind20();
	void OneStep();
	void estprewhitening();
	void estsetbisfree();
	void modelfun(int colnum);
	matrix<double> cholderiv(const matrix<double> A);
	void cholderiv(const matrix<double> A, std::vector<matrix<double>> AD, matrix<double>* R, std::vector<matrix<double>> *Rd);

	///X - input marix, colnum - number of using column, dat - name of input  matrix(false = 'dat', true = 'datcy')
	matrix<double> prewhiten(matrix<double> X, int colnum, bool dat); 
	void calc_Xs();
	matrix<double> cymatfilt(matrix<double>b, matrix<double> datcy, string method);
	matrix<double> cymatfull(matrix<double> x);   //equal cymatdat
	matrix<double> cymatdatcyclic(matrix<double> x);
	matrix<double> cymatdatcyclic(matrix<double> x, matrix<double> i);
	void postfun(matrix<double> eta);
	void calc_Ja();
	void calc_Jd();

	struct prewhitenstartmatrixreturnvalues { matrix<double> Ds; matrix<double> DPoly; matrix<double> Dextra; matrix<double> Dsderiv; std::vector<matrix<double>> Dsderiv3D; matrix<double> DsderivNumeric; };
	prewhitenstartmatrixreturnvalues prewhitenstartmatrix(matrix<double> Dpoly, string method);
	struct delaypolyreturnvalues { matrix<double> Ld; double Ld_Td; double Ld_TdTd; };
	delaypolyreturnvalues*  delaypoly(matrix<double> Td, matrix<double> dt, string LdMode);
	matrix<double> bs2coeff(matrix<double> bs);
	matrix<double> mldivideminus(matrix<double> *x, matrix<double> *y, matrix<double> *ibad);

	matrix<double>  readfile(string file);
	void supdate();

	matrix<double> diffbackward(matrix<double> d);
	matrix<double> diffforward(matrix<double> d);
	
	matrix<bool> ismember(matrix<double> A, matrix <double> B);
	matrix <double> filter(const boost::numeric::ublas::vector<double> Blinkfilt,const matrix<double>a,const matrix<double>x);		//use only for 1 column matrix!!!!
	matrix<double> sum(matrix<double> x);
	void estxnmake();
	void ypreprocess7();
	matrix<double> delay(const matrix<double> x); 
	matrix<double> delay(const matrix<double> x, const matrix<double> k);
	matrix<double> delaycy(const matrix<double> x, matrix<double>lag);
	matrix<double> delaycy(const matrix<double> x, matrix<double>lag, matrix<double> i);
	matrix<double> trtimesminus(matrix<double> x, matrix<double>y, matrix<double> ibad);
private:

	void strtomatrix(string str, matrix <double> *mat);
	void readtxt(string filename);
	void removeSpaces(std::string& str);
	unsigned int split(std::string &txt, std::vector<std::string> &strs, char ch);



};


